<?php get_header(); ?>
    	<section class="sub-banner" style="background-image: url(/wp-content/uploads/2017/06/generic-header-3.jpg); position: relative;">
		<div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2><?php printf( __( 'Search Results for: %s', 'rpm' ), get_search_query() ); ?></h2>
					</div>
				</div>
			</div>
		</section>
		<section class="white-bg blog-section">
			<div class="container">
				<div class="row one-blog-section">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="clearfix" style="margin-bottom: 50px;">
							<?php if (has_post_thumbnail()) { ?>
								<div class="col-md-6" data-aos="fade-in-up" data-aos-duration="1000">
									<a href="<?php the_permalink(); ?>">
										<figure style="background: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-position: center;">
										</figure>
									</a>
								</div>
								<div data-aos="fade-in-up" data-aos-duration="1000" id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 single-post-blog text-left'); ?>>
							<?php } else { ?>
								<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 single-post-blog text-left'); ?>>
							<?php } ?>
						            <h2><?php the_category(', '); ?></h2>
						            <h1 class="header__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
						            <?php the_excerpt(); ?>
						            <a href="<?php the_permalink(); ?>" class="black-btn">read the rest</a>
						        </div>
				       </div>
				    <?php endwhile;
				    else :
				    	echo '<div class="col-md-12 text-center"><h1>No results found...</h1></div>';
				    endif; ?>

				</div>
			</div>
		</section>
		<?php include 'template-part-bottom-nav.php'; ?>
<?php get_footer(); ?>