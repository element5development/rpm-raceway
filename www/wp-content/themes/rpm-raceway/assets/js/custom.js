jQuery(function($) {
	var visitorLocation = $.cookie('visitorLocation');

	if ( ( 'undefined' != typeof visitorLocation ) && ( visitorLocation.length > 0 ) &&  'Stamford' === visitorLocation ) {
		$('body').append('<script async src="https://tag.simpli.fi/sifitag/81a92ee0-d93d-0136-46e2-06659b33d47c"></script>');
	}
});
