	<footer class="main-footer">
		<div class="container footer-top">
			<div class="row">
				<div class="col-sm-8">
					<img src="<?php bloginfo('template_url') ?>/assets/images/footerlogo.png">
				</div>
				<div class="col-sm-4 footer-social">
					<a href="<?php echo ot_get_option( 'facebook_link' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="<?php echo ot_get_option( 'instagram_link' ); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="<?php echo ot_get_option( 'youtube_link' ); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
					<a href="<?php echo ot_get_option( 'twitter_link' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
				</div>
			</div>
		</div>
		<div class="container footer-widget">
			<div class="row">
				<?php dynamic_sidebar('footer-subscription-area'); ?>
				<div class="col-sm-1">
				</div>
				<?php dynamic_sidebar('footer-widget-area'); ?>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-sm-5">
					<h3>CONTACT US</h3>
					<a href="mailto:<?php echo ot_get_option( 'email_address' ); ?>"><i class="fa fa-envelope"></i> <?php echo ot_get_option( 'email_address' ); ?></a><br>
					<a href="tel:<?php echo ot_get_option( 'phone_number' ); ?>"><i class="fa fa-phone"></i> <?php echo ot_get_option( 'phone_number' ); ?></a>
				</div>
				<div class="col-sm-1">
				</div>
				<?php dynamic_sidebar('stay-connected-embed-code'); ?>
			</div>
		</div>
		<div class="container footer-copyrights">
			<div class="row">
				<div class="col-sm-6">
					© 2017 RPM Raceway. All rights reserved.
				</div>
				<div class="col-sm-6">
					<ul>
	                    <?php
	                        wp_nav_menu(array(
	                            'theme_location' => 'copyrights-menu',
	                            'container'      => '',
	                            'items_wrap'    => '%3$s'
	                        ));
	                     ?>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.bxslider.js"></script>
	<script type="text/javascript">
	    $(document).ready(function(){
	    });
	</script>
	<?php wp_footer(); ?>
</body>
</html>