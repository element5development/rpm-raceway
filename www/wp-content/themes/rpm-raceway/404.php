<html style="height: 100%; overflow: hidden;">
<head>
	<title>RPM Raceway - 404 - Not Found</title>
	<link rel="shortcut icon" href="/wp-content/themes/rpm-raceway/assets/images/rpm-favicon.png" type="image/x-icon">
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css?family=Droid+Sans:400,700');
		* {
			font-family: 'Droid Sans';
		}
		a:hover {
			background: #d81212 !important;
			border-color: #d81212 !important;
			color: #fff !important;
		}
	</style>
</head>
<body style="background: #000; height: 100%; display: flex; width: 100%; align-items: center; overflow: hidden;">
<div style="align-items: center; text-align: center; width: 100%;">
	<img class="logo-sticky" style="max-width: 500px;margin-bottom: 40px;" src="/wp-content/themes/rpm-raceway/assets/images/long-logo.svg">
	<h1 style="color: #d81212; text-transform: uppercase; margin-top: 0px; font-size: 50px; font-weight: bold; ">404 - Not Found</h1>
	<h2 style="color: #fff; font-size: 24px; font-weight: normal; margin-bottom: 45px;">The page you are looking for is not found.</h2>
	<a href="/" style="display: block; width: 200px; padding: 15px 20px; text-decoration: none; font-size: 18px; text-align: center; border: 2px solid #fff; color: #000; background: #fff;  margin: 0 auto; font-weight: bold;">GO BACK TO HOME</a>
</div>
</body>
</html>