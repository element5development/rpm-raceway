<?php
/* Template Name: Go Kart and Tracks */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 10)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading">RACING AT RPM</h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/flag.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider" data-aos="fade-up" data-aos-duration="1000">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
			            <div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')";></div>
					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<?php if(get_field('rpm_go_karts_content')) { ?>
		<a href="#" style="margin: -50px 0 50px 0; display: block;" id="go-karts"></a>
		<section class="rules" data-aos="fade-in" data-aos-duration="1000">
			<div class="container">
				<div class="text">
					<h4>rpm go-karts</h4>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
					</figure>
					<?php the_field('rpm_go_karts_content'); ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('racing_boxes') ) { ?>
		<section class="grey-bg three-column karts-sec">
			<div class="container text-center">
			<?php while( have_rows('racing_boxes') ) { the_row(); ?>
				<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php if (get_sub_field('title')) { ?>
						<h3><?php the_sub_field('title'); ?></h3>
					<?php } ?>
					<p><?php the_sub_field('text'); ?></p>
					<?php if (get_sub_field('button_link')) { ?>
						<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		</section>
		<?php } ?>

		<?php if(get_field('parallax_1_background_image')) { ?>
		<section class="parallax" data-aos="fade-up" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_1_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_1_heading'); ?></h2>
					<a href="<?php the_field('parallax_1_button_link'); ?>" class="button red"><span><?php the_field('parallax_1_button_text'); ?></span></a>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('tracks') ) { ?>
		<a href="#" style="margin: -50px 0 50px 0; display: block;" id="track-map"></a>
		<section class="track-map">
			<div class="text-center">
				<h4>Track Map</h4>
				<figure class="line-break">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
				</figure>
				<div class="map-slider">
					<?php while( have_rows('tracks') ) { the_row(); ?>
						<div class="slider-section" style="display: none;"
                             data-location="<?php the_sub_field('title') ?>">
							<figure style="text-align: center;"><img src="<?php the_sub_field('image'); ?>"></figure>
							<div class="content">
								<h3><?php the_sub_field('title'); ?></h3>
								<p><?php the_sub_field('text'); ?></p>
							</div>
						</div>
					<?php } ?>
				</div>
				<!--<div id="bx-pager">
					<?php $sl=0; while( have_rows('tracks') ) { the_row(); ?>
						<a data-slide-index="<?php echo $sl; ?>" href=""><img src="<?php echo (get_sub_field('small_image')) ? get_sub_field('small_image') : get_sub_field('image'); ?>" /><?php the_sub_field('title') ?></a>
					<?php $sl++; } ?>
				</div>-->
			</div>
		</section>
		<?php } ?>

		<?php if(get_field('parallax_2_background_image')) { ?>
		<section class="parallax parallax-big" data-aos="fade-in" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_2_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_2_heading'); ?></h2>
					<p><?php the_field('parallax_2_content'); ?></p>
					<a href="<?php the_field('parallax_2_button_link'); ?>" class="button red"><span><?php the_field('parallax_2_button_text'); ?></span></a>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>