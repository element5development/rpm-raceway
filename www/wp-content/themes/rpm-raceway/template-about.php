<?php
/* Template Name: About */
get_header(); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="rules littleBigWrapper" style="padding-bottom: 0;">
			<div class="container">
				<div id="post-<?php the_ID(); ?>" <?php post_class('text'); ?>>
					<h4>ABOUT RPM RACEWAY</h4>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
					</figure>
					<?php if( have_rows('team_members') ) { ?>
	            		<div class="row teamMembers" style="display: none;">
						<?php while( have_rows('team_members') ) { the_row(); ?>
	            			<div class="col-md-6 text-center">
	            				<div style="border: 2px solid gray;">
		            				<h3 style="font-family: 'Bebas Neue'; font-size: 30px; margin: 0 auto 15px auto; border-bottom: 1px solid gray; background: #e2e2e2; padding: 15px;"><?php the_sub_field('name'); ?></h3>
		            				<img src="<?php the_sub_field('image'); ?>" class="img-responsive" style="display: block; margin: 0 auto;">
		            				<h4 style="font-family: 'Droid Sans'; font-size: 22px; margin: 15px auto 0 auto; border-top: 1px solid gray; background: #e2e2e2; padding: 15px;"><?php the_sub_field('title'); ?></h4>
	            				</div>
	            			</div>
						<?php } ?>
	            		</div>
					<?php } ?>
            		<br>
            		<br>
            		<div class="row">
            			<div class="col-md-12 normalContent">
            			<?php the_content(); ?>
            			</div>
            		</div>
				</div>
				<!-- <br>
				<br>
				<br>
				<br>
				<h4>PRESS RELEASE</h4>
				<figure class="line-break">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
				</figure> -->
			</div>
		</section>

		<section class="white-bg blog-section" style="margin-top:0; display: none;">
			<div class="container" style="max-width:1040px;">
				<div class="row">
					<div class="location-heading clearfix" style="margin-top: 20px; margin-bottom: 20px; max-width: 100%;">
						<a id="pressAll" href="javascript:;" style="position: absolute; right: 0px; top: 50px; font-size: 14px; color: #000;">Show All</a>
						<?php include 'template-part-location.php'; ?>
					</div>
				</div>
				<br>
				<div class="row one-blog-section">
<?php
	query_posts('post_per_page=-1&post_type=press-room');
		while ( have_posts() ) { the_post();  ?>

						<?php if (has_post_thumbnail(get_the_ID())) { ?>
							<div class="clearfix <?php echo 'press-re '.get_field('location', get_the_ID()); ?>">
							<div class="col-md-6" data-aos="fade-in-up" data-aos-duration="1000">
								<a href="<?php the_permalink(); ?>">
									<figure style="background: url(<?php the_post_thumbnail_url(get_the_ID()); ?>); background-size: cover; background-position: center;">
									</figure>
								</a>
							</div>
							<div data-aos="fade-in-up" data-aos-duration="1000" id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 single-post-blog text-left press-re '.get_field('location', get_the_ID())); ?>>
						<?php } else { ?>
							<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 single-post-blog text-left press-re '.get_field('location', get_the_ID())); ?>>
						<?php } ?>
					            <h2><?php the_category(', '); ?></h2>
					            <h1 class="header__title"><a href="<?php the_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></h1>
					            <?php the_excerpt(get_the_ID()); ?>
					            <a href="<?php the_permalink(get_the_ID()); ?>" class="black-btn">read the rest</a>
					        </div>
					       </div>
	<?php	}
	wp_reset_query();
?>


				</div>
			</div>
		</section>
		<style type="text/css">.press-re {margin-bottom: 50px;}</style>
		<script type="text/javascript">
			jQuery(function($){
				$('.locationStuff h3').html('ALL');
				$('#pressAll').click(function(){
					$('.press-re').attr('style', 'display: block;');
					$('.locationStuff h3').html('ALL');
				});
			});
		</script>
		<section class="our-partner">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h4>Our partners</h4>
						<ul class="partner">
							<?php if( have_rows('partners', 4) ) { ?>
								<?php while( have_rows('partners', 4) ) { the_row(); ?>
									<li><img src="<?php the_sub_field('image', 4); ?>"></li>
								<?php } ?>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
    <?php endwhile; endif; ?>

<?php get_footer(); ?>