<?php
/* Template Name: Calendar */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="white-bg calendar four-column">
			<div class="container text-center">
				<div class="row">
					<div class="location-heading clearfix">
						<?php include 'template-part-location.php'; ?>
					</div>
					<div class="calendar-section" data-aos="fade-up" data-aos-duration="1000" style=" height: 470px; background-color: #000000;">
						<div class="calendar">
							<div class="calendarPage JerseyCity">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Jersey City"]'); ?>
							</div>
							<div class="calendarPage LongIsland">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Long Island"]'); ?>
							</div>
							<div class="calendarPage Stamford">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Stamford"]'); ?>
							</div>
							<div class="calendarPage Buffalo">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Buffalo"]'); ?>
							</div>
							<div class="calendarPage Rochester">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Rochester"]'); ?>
							</div>
							<div class="calendarPage Syracuse">
								<?php echo do_shortcode('[eo_fullcalendar responsive=0 headerLeft="prev" headerCenter="title" headerRight="next" venue="Syracuse"]'); ?>
							</div>
						</div>
						<div class="events">
							<style type="text/css">.eo-events.eo-events-shortcode li, .eo-events.eo-events-shortcode {margin: 0px; padding: 0px;}</style>
							<div></div>
							<div class="calendarPage JerseyCity">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Jersey City"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
							<div class="calendarPage LongIsland">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Long Island"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
							<div class="calendarPage Stamford">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Stamford"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
							<div class="calendarPage Buffalo">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Buffalo"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
							<div class="calendarPage Rochester">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Rochester"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
							<div class="calendarPage Syracuse">
								<?php echo do_shortcode('[eo_events orderby="eventstart" order="ASC" numberposts="1" showpastevents="false" venue="Syracuse"]<h3>%start{M}%</h3><h2>%start{j}%</h2><span class="hideMobileSpan">, </span><h3>%start{Y}%</h3><h4>%event_title%</h4><span>%start{g:i a}% - %end{g:i a}%</span><p>%event_excerpt{20}%</p>[/eo_events]'); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="row" style="margin-bottom: 60px; display: none;"  data-aos="zoom-in" data-aos-duration="1000">
					<h4>weekly events</h4>
					<figure class="line-break"><img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png"></figure>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar1.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar2.png">
						</figure>
						<div class="content-box">
							<h3>Adult Racing League</h3>
							<p>Maecenas sed diam eget risus varius blandit sit amet non magna. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
							<p>Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.</p>
							<p>Donec ullamcorper nulla non metus auctor fringilla.</p>
							<a href="javascript:;" class="black-btn">inquire now</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar3.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar4.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
				</div>
				<div class="row"  style="margin-bottom: 60px; display: none;" data-aos="zoom-in" data-aos-duration="1000">
					<h4>SPECIAL EVENTS</h4>
					<figure class="line-break"><img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png"></figure>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar1.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar2.png">
						</figure>
						<div class="content-box">
							<h3>Adult Racing League</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">inquire now</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar3.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
					<div class="box">
						<figure>
							<img src="<?php bloginfo('template_url'); ?>/assets/images/calendar3.png">
						</figure>
						<div class="content-box">
							<h3>Catering</h3>
							<p>(7 person minimum) <br>Includes: Use of helmet & head sock, Printed race result sheet, Email newsletter offers, Valid for one year </p>
							<a href="javascript:;" class="black-btn">find out more</a>
						</div>
					</div>
				</div>
				<!-- <a href="javascript:;" class="black-btn">load more</a> -->
			</div>
		</section>

		<?php if (get_field('parallax_background_image')) { ?>
		<section class="parallax" data-aos="fade-up" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_heading'); ?></h2>
					<a href="<?php the_field('parallax_button_link'); ?>" class="button red"><span><?php the_field('parallax_button_text'); ?></span></a>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<script type="text/javascript">
	jQuery(function($){
		$('.events .eo-events.eo-events-shortcode').each(function(){
			var Evlen = $(this).find('li').length;
			if (Evlen == 0) {
				$(this).html('<h2 style="padding-top: 90px;">No Upcoming Event</h2>');
			}
			if (Evlen > 1) {
				// $(this).find('li:not(:last-child)').remove();
			}
		});
	});
</script>
<?php get_footer(); ?>