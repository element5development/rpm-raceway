<?php include 'header.php'; ?>

	<?php
		$bannerImage = (get_field('banner_image', 14)) ? 'background-image: url('.get_field('banner_image', 14).');' : 'background: black;';
		$bannerText = (get_field('banner_text', 14)) ? get_field('banner_text', 14) : '';
		if (basename(get_page_template()) === 'page.php') {
			$bannerText = (get_field('banner_text', 14)) ? get_field('banner_text', 14) : get_the_title();
		}
	?>
	<section class="sub-banner" style="<?php echo $bannerImage; ?>; position: relative;">
		<div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php if ($bannerText != '') { ?>
						<h2><?php echo $bannerText; ?></h2>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>


	<section class="white-bg shop-section">
		<div class="container">
			<h2 class="text-center">SHOP <span>RPM RACEWAY</span></h2>