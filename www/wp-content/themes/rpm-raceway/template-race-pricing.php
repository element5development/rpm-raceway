<?php
/* Template Name: Race Pricing */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 10)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading">RACING AT RPM</h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="nav-tabs">
			<div class="container text-center">
				<ul class="nav">
					<li class="active nav2"><a href="#2"  data-toggle="tab" class="tabs-btn">groups and parties</a></li>
					<li class="nav1"><a href="#1" data-toggle="tab" class="tabs-btn">arrive and drive</a></li>
					<li class="nav3"><a href="#3"  data-toggle="tab" class="tabs-btn">corporate events</a></li>
					<!-- <li><a href="#4"  data-toggle="tab" class="tabs-btn">public events</a></li>
					<li><a href="#5"  data-toggle="tab" class="tabs-btn">specials</a></li> -->
				</ul>
			</div>
		</section>
		<section class="tab-content ">
			<div class="tab-pane" id="1">
				<section class="parties-content">
					<div class="container text-center">
						<?php the_field('arrive_&_drive_title'); ?>
					</div>
				</section>
				<section class="white-bg four-column corporate-events parties">
					<div class="container text-center">
						<div class="row">
							<div class="location-heading clearfix" style="margin-top: 20px; margin-bottom: 0px;">
								<?php include 'template-part-location.php'; ?>
							</div>

						</div>
					</div>
				</section>

                <!-- Member Adult/Non-License Races # 1 -->
				<section class="white-bg four-column corporate-events parties">
					<div class="container text-center">
						<h4><?php the_field('arrive_&_drive_pricing_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('arrive_&_drive_pricing_cards') ) { ?>
								<?php while( have_rows('arrive_&_drive_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2 <?php echo (get_sub_field('heading') == 'Annual Membership') ? 'class="'.get_sub_field('location').'AnnualPrices"' : '' ; ?>><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
						<p class="text-center reserve">Annual membership required. No reservations required.</p>
					</div>
				</section>

                <!-- Member Junior Pricing  # 1 -->
				<section class="white-bg four-column corporate-events parties">
					<div class="container text-center">
						<h4><?php the_field('race_category_5_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('race_category_5_pricing_cards') ) { ?>
								<?php while( have_rows('race_category_5_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>

                <!-- Annual Membership  # 1 -->
				<section class="content-with-image inner-pages clearfix">
					<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
						<figure class="full-image" style="background-image: url(<?php the_field('annual_membership_left_image'); ?>);">
						</figure>
					</div>
					<div class="right-content dark-content" style="background: black;" data-aos="fade-left" data-aos-duration="1000">
						<div class="content">
							<h4><?php the_field('annual_membership_heading'); ?> <span class="price priceAnnual"><?php the_field('annual_membership_price'); ?></span></h4>
							<figure class="line-break">
								<img src="/wp-content/themes/rpm-raceway/assets/images/line-break-small-red.png">
							</figure>
							<?php the_field('annual_membership_content'); ?>
						</div>
					</div>
				</section>

                <!-- Adult/Non-License/Junior Non-Member Race Bundles  # 1 -->
				<section class="white-bg four-column corporate-events parties small-box">
					<div class="container text-center">
						<h4><?php the_field('race_category_1_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('race_category_1_pricing_cards') ) { ?>
								<?php while( have_rows('race_category_1_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
						<br>
						<br>
						<p>Race Price includes use of Helmet & Head Sock.</p>
					</div>
				</section>

				<?php if( have_rows('race_driver_categories', 10) ) { ?>
				<section class="driver-cat pricing">
					<div class="container">
						<?php while( have_rows('race_driver_categories', 10) ) { the_row(); ?>
							<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
								<h3><?php the_sub_field('title', 10); ?></h3>
								<?php the_sub_field('points', 10); ?>
								<figure>
									<img src="<?php the_sub_field('image', 10); ?>">
								</figure>
							</div>
						<?php } ?>
					</div>
				</section>
				<?php } ?>

			</div>
			<div class="tab-pane active" id="2">
				<section class="parties-content">
					<div class="container text-center">
						<?php the_field('groups_&_parties_pricing_content'); ?>
					</div>
                    <!--<div class="row">
                        <div class="location-heading clearfix">
                            <?php include 'template-part-location.php'; ?>
                        </div>
                    </div>-->
				</section>
				<section class="white-bg four-column corporate-events parties">
					<div class="container text-center">
						<div class="row">
							<div class="location-heading clearfix" style="margin-top: 20px; margin-bottom: 0px;">
								<?php include 'template-part-location.php'; ?>
							</div>
						</div>
					</div>
				</section>

                <!-- Racing events #2 -->
				<section class="white-bg four-column corporate-events parties small-box">
					<div class="container text-center">
						<h4><?php the_field('race_category_2_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('race_category_2_pricing_cards') ) { ?>
								<?php while( have_rows('race_category_2_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>

                <!-- Birthday Party packages #2 -->
				<section class="white-bg four-column corporate-events parties small-box">
					<div class="container text-center">
						<h4><?php the_field('race_category_4_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('race_category_4_pricing_cards') ) { ?>
								<?php while( have_rows('race_category_4_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>

                <!-- Bachelor/Bachelorette Party Packages #2 -->
				<section class="white-bg four-column corporate-events parties small-box">
					<div class="container text-center">
						<h4><?php the_field('race_category_3_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('race_category_3_pricing_cards') ) { ?>
								<?php while( have_rows('race_category_3_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>

                <!-- Group and Parties packages #2 [doubtful may be duplicate content] -->
				<section class="white-bg four-column corporate-events parties small-box">
					<div class="container text-center">
						<!-- <h4>Group and Parties packages</h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure> -->
						<div class="row grid">
							<?php if( have_rows('groups_&_parties_pricing_cards') ) { ?>
								<?php while( have_rows('groups_&_parties_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>

			</div>
			<div class="tab-pane" id="3">
				<section class="parties-content">
					<div class="container text-center">
						<?php the_field('corporate_events_pricing_content'); ?>
					</div>
				</section>
				<section class="white-bg four-column corporate-events parties masonry">
					<div class="container text-center">
						<div class="row">
							<div class="location-heading clearfix">
								<?php include 'template-part-location.php'; ?>
							</div>
						</div>
                    </div>
                </section>

<?php
	// $corporate_events_price_arr = get_field('corporate_events_pricing_cards');
	// usort($corporate_events_price_arr, function($a, $b) { // anonymous function
	//     return strcmp(strtoupper($a['price_category']), strtoupper($b['price_category']));
	// });
?>
                <section class="white-bg four-column corporate-events parties">
					<div class="container text-center">
						<h4><?php the_field('corporate_events_pricing_heading'); ?></h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('corporate_events_pricing_cards') ) { ?>
								<?php while( have_rows('corporate_events_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
                </section>
			</div>
			<div class="tab-pane" id="4" style="display: none;">
				<section class="parties-content">
					<div class="container text-center">
						<?php the_field('public_events_pricing_content'); ?>
					</div>
				</section>
				<section class="white-bg four-column corporate-events parties masonry">
					<div class="container text-center">
						<div class="row">
							<div class="location-heading clearfix">
								<?php include 'template-part-location.php'; ?>
							</div>
						</div>
						<h4>PUBLIC EVENTS packages</h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php if( have_rows('public_events_pricing_cards') ) { ?>
								<?php while( have_rows('public_events_pricing_cards') ) { the_row(); ?>
									<div class="box grid-item <?php echo get_sub_field('location'); ?>">
										<?php if (get_sub_field('image')) { ?>
											<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<?php if (get_sub_field('price_category')) { ?>
												<p class="text-center"><?php the_sub_field('price_category'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('heading')) { ?>
												<h3><?php the_sub_field('heading'); ?></h3>
											<?php } ?>
											<?php if (get_sub_field('price')) { ?>
												<h2><?php the_sub_field('price'); ?></h2>
											<?php } ?>
											<?php if (get_sub_field('text_under_price')) { ?>
												<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
											<?php } ?>
											<?php if (get_sub_field('content')) { ?>
												<?php the_sub_field('content'); ?>
											<?php } ?>
											<?php if (get_sub_field('button_link')) { ?>
												<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
											<?php } ?>
											<?php if (get_sub_field('text_under_button')) { ?>
												<p><small><?php the_sub_field('text_under_button'); ?></small></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</section>
			</div>
			<div class="tab-pane" id="5" style="display: none;">
				<section class="parties-content">
					<div class="container text-center">
						<?php the_field('specials_pricing_content'); ?>
					</div>
				</section>
				<section class="white-bg four-column corporate-events parties masonry">
					<div class="container text-center">
						<div class="row">
							<div class="location-heading clearfix">
								<?php include 'template-part-location.php'; ?>
							</div>
						</div>
						<h4>SPECIALS</h4>
						<figure class="line-break">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
						</figure>
						<div class="row grid">
							<?php $specialsQ = new WP_Query( array('post_type' => 'rpm_current_specials', 'posts_per_page' => -1 ) );
							if ( $specialsQ->have_posts() ) {  while ( $specialsQ->have_posts() ) { $specialsQ->the_post(); ?>
									<div class="box grid-item JerseyCity LongIsland Buffalo Rochester Syracuse">
										<?php if (get_the_post_thumbnail_url()) { ?>
											<figure><img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-responsive"></figure>
										<?php } ?>
										<div class="content-box">
											<h3><?php echo get_the_title(); ?></h3>
											<?php if (get_the_content()) { ?>
												<p class="text-center"><?php the_content(); ?></p>
											<?php } ?>
										</div>
									</div>
								<?php } ?>
							<?php } wp_reset_query(); ?>
						</div>
					</div>
				</section>
			</div>
		</section>
		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<script type="text/javascript">
	jQuery(function($){
		<?php if (isset($_GET['group'])) { ?>
			$('.nav1, #1').removeClass('active');
			$('.nav2, #2').addClass('active');
		<?php } ?>
		<?php if (isset($_GET['corporate'])) { ?>
			$('.nav1, #1').removeClass('active');
			$('.nav3, #3').addClass('active');
		<?php } ?>
	});
</script>

<?php get_footer(); ?>