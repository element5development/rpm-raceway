<?php
/* Template Name: Gift-Cards */
get_header(); ?>
<html>
<head>
<style>
@media (min-width: 320px) and (max-width: 480px) {
   
  text-align:center !important;
  margin-top:-18px !important;
 
}
</style>
</head>
</html>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>

        
        <section class="white-bg four-column corporate-events parties">
            <div class="container text-center">
                <div class="row">
                    <div class="location-heading clearfix" style="margin-top: 20px; margin-bottom: 0px;">
                        <?php include 'template-part-location.php'; ?>
                    </div>

                </div>
            </div>
             <p style="text-align:center;margin-top:-18px;white-space:pre-wrap;">If you're having issues purchasing and are using Safari on an older iOS device,
      please try Chrome instead for the best experience.</p>
        </section>
    

        <section class="tab-content ">
            <div class="tab-pane active" id="1">
                <iframe id="giftcards" src="" width="100%" height="800" frameborder="0" ></iframe>
            </div>
        </section>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>