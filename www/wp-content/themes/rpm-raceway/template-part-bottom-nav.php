<section  data-aos="fade-in-up" data-aos-duration="1000">
	<div class="container text-center">
		<div class="circle-group clearfix no-margin">
			<div class="circle black <?php if(strpos($_SERVER['REQUEST_URI'], 'race') !== false){ echo "active";} ?>">
				<a href="/race">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/flag.png" alt="">
					<h6>Racing</h6>
				</a>
			</div>
			<div class="circle black <?php if(strpos($_SERVER['REQUEST_URI'], 'group-events') !== false){ echo "active";} ?>">
				<a href="/group-events">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/users.png" alt="">
					<h6>Group events</h6>
				</a>
			</div>
			<div class="circle black <?php if(strpos($_SERVER['REQUEST_URI'], 'eat-drink') !== false){ echo "active";} ?>">
				<a href="/eat-drink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/drink-icon.png" alt="">
					<h6>eat &amp; drink</h6>
				</a>
			</div>
			<div class="circle black <?php if(strpos($_SERVER['REQUEST_URI'], 'play') !== false){ echo "active";} ?>">
				<a href="/play">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/gaming-icon.png" alt="">
					<h6>Play</h6>
				</a>
			</div>
			<div class="circle black <?php if(strpos($_SERVER['REQUEST_URI'], 'calendar') !== false){ echo "active";} ?>">
				<a href="/calendar">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/calander.svg" alt="">
					<h6>Calendar</h6>
				</a>
			</div>
		</div>
	</div>
</section>