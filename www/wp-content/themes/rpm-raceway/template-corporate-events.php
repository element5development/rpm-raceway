<?php
/* Template Name: Corporate Events */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 11)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading"><?php echo get_the_title(11); ?></h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/users.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>

		<?php if( get_field('event_content') ) { ?>
		<section class="grey-bg corporate-events one-column" data-aos="fade-in" data-aos-duration="1000">
			<div class="container text-center">
				<h4><?php the_field('event_content_heading'); ?></h4>
				<figure class="line-break">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
				</figure>
				<div class="box">
					<?php the_field('event_content'); ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider" data-aos="fade-up" data-aos-duration="1000">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
						<div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')"></div>
						<!-- <div class="item parallax-window" data-parallax="scroll" data-image-src="<?php //echo get_sub_field('image'); ?>"></div> -->

					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<section class="white-bg four-column corporate-events parties masonry">
			<div class="container text-center">
				<div class="row">
					<div class="location-heading clearfix">
						<?php include 'template-part-location.php'; ?>
					</div>
				</div>
				<div class="row grid">
					<?php if( have_rows('corporate_events_pricing_cards') ) { ?>
						<?php while( have_rows('corporate_events_pricing_cards') ) { the_row(); ?>
							<div class="box grid-item <?php echo get_sub_field('location'); ?>">
								<?php if (get_sub_field('image')) { ?>
									<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
								<?php } ?>
								<div class="content-box">
									<?php if (get_sub_field('heading')) { ?>
										<h3><?php the_sub_field('heading'); ?></h3>
									<?php } ?>
									<?php if (get_sub_field('price')) { ?>
										<h2><?php the_sub_field('price'); ?></h2>
									<?php } ?>
									<?php if (get_sub_field('text_under_price')) { ?>
										<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
									<?php } ?>
									<?php if (get_sub_field('content')) { ?>
										<?php the_sub_field('content'); ?>
									<?php } ?>
									<?php if (get_sub_field('button_link')) { ?>
										<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
									<?php } ?>
									<?php if (get_sub_field('text_under_button')) { ?>
										<p><small><?php the_sub_field('text_under_button'); ?></small></p>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</section>

		<?php if (is_page('kids-parties')): ?>
        <!-- Birthday Party packages #2 -->
        <section class="white-bg four-column corporate-events parties small-box">
            <div class="container text-center">
                <h4><?php $field = get_field('race_category_4_heading', '81', true);echo $field; ?></h4>
                <figure class="line-break">
                    <img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
                </figure>
                <div class="row grid">
                    <?php if( have_rows('race_category_4_pricing_cards','81') ) { ?>
                        <?php while( have_rows('race_category_4_pricing_cards','81') ) { the_row(); ?>
                            <div class="box grid-item <?php echo get_sub_field('location'); ?>">
                                <?php if (get_sub_field('image')) { ?>
                                    <figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
                                <?php } ?>
                                <div class="content-box"  style="margin-bottom:40px;">
                                    <?php if (get_sub_field('price_category')) { ?>
                                        <p class="text-center"><?php the_sub_field('price_category'); ?></p>
                                    <?php } ?>
                                    <?php if (get_sub_field('heading')) { ?>
                                        <h3><?php the_sub_field('heading'); ?></h3>
                                    <?php } ?>
                                    <?php if (get_sub_field('price')) { ?>
                                        <h2><?php the_sub_field('price'); ?></h2>
                                    <?php } ?>
                                    <?php if (get_sub_field('text_under_price')) { ?>
                                        <p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
                                    <?php } ?>
                                    <?php if (get_sub_field('content')) { ?>
                                        <?php the_sub_field('content'); ?>
                                    <?php } ?>
                                    <?php if (get_sub_field('button_link')) { ?>
                                        <a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
                                    <?php } ?>
                                    <?php if (get_sub_field('text_under_button')) { ?>
                                        <p><small><?php the_sub_field('text_under_button'); ?></small></p>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
		<?php endif ?>

		<?php if(get_field('parallax_background_image')) { ?>
		<section class="parallax" data-aos="fade-up" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_heading'); ?></h2>
					<a href="<?php the_field('parallax_button_1_link'); ?>" class="button red"><span><?php the_field('parallax_button_1_text'); ?></span></a>
					<?php if (get_field('parallax_button_2_link')) { ?>
						<a href="<?php the_field('parallax_button_2_link'); ?>" class="button black"><span><?php the_field('parallax_button_2_text'); ?></span></a>
					<?php } ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php include 'template-part-reviews.php'; ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>