<?php
  if (!isset($_COOKIE['visitorLocation']) || !isset($_COOKIE['visitorLocationClass'])) {
    $visitorLocation = json_decode(file_get_contents('http://freegeoip.net/json/'.getUserIP()));
    global $wpdb;
    $results = $wpdb->get_results( 'SELECT * FROM wp_rpm_locations', OBJECT );
    $distance = array();
    foreach ($results as $key => $value) {
      $distance[$key] = array(distance($visitorLocation->latitude, $visitorLocation->longitude, $value->lat, $value->long, "M") => $value->name);
    }
    $visitorLocation = implode(" ", min($distance));
    setcookie('visitorLocation', $visitorLocation, time()+86400, "/");
    $_COOKIE['visitorLocation'] = $visitorLocation;
    $visitorLocationClass = '.'.str_replace(' ', '', $visitorLocation);
    setcookie('visitorLocationClass', $visitorLocationClass, time()+86400, "/");
    $_COOKIE['visitorLocationClass'] = $visitorLocationClass;
  }
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<link rel="shortcut icon" href="/wp-content/themes/rpm-raceway/assets/images/rpm-favicon.png" type="image/x-icon">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/aos.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-‎101732405-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<section class="top-header clearfix">
		<div class="pull-left">
			<div class="tel desktop">
<!-- 				<a href="tel:<?php //echo ot_get_option( 'phone_number' ); ?>">
					<img src="<?php //bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt=""><?php //echo ot_get_option( 'phone_number' ); ?>
				</a> -->
				<a href="tel:201-333-7223" class="JerseyCity headerLocationLink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt="">201-333-7223
				</a>
				<a href="tel:631-752-7223" class="LongIsland headerLocationLink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt="">631-752-7223
				</a>
				<a href="tel:716-683-7223" class="Buffalo headerLocationLink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt="">716-683-7223
				</a>
				<a href="tel:585-427-7223" class="Rochester headerLocationLink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt="">585-427-7223
				</a>
				<a href="tel:315-423-7223" class="Syracuse headerLocationLink">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/phone-icon.png" alt="">‎315-423-7223
				</a>
			</div>
		</div>
		<div class="pull-right">
			<div class="location">
				<ul>
					<li>Your location:</li>
					<li class="locationHeaderStuffHeading"><?php echo (isset($_COOKIE['visitorLocation'])) ? $_COOKIE['visitorLocation'] : ''; ?></li>
					<li class="locationStuff">
						 <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
					 	<ul class="dropdown-menu">
						 	<li><a href="javascript:;" data-location=".JerseyCity">Jersey City</a></li>
						 	<li><a href="javascript:;" data-location=".LongIsland">Long Island</a></li>
						 	<li><a href="javascript:;" data-location=".Buffalo">Buffalo</a></li>
						 	<li><a href="javascript:;" data-location=".Rochester">Rochester</a></li>
						 	<li><a href="javascript:;" data-location=".Syracuse">Syracuse</a></li>
                        </ul>
					</li>
					<li>
						<a href="http://maps.google.com/?q=99 Caven Point Road, Jersey City, NJ 07305" target="_blank" class="JerseyCity headerLocationLink">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png" alt="">
						</a>
						<a href="http://maps.google.com/?q=40 Daniel St. Farmingdale, NY 11735" target="_blank" class="LongIsland headerLocationLink">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png" alt="">
						</a>
						<a href="http://maps.google.com/?q=One Walden Galleria, Buffalo, NY 14225" target="_blank" class="Buffalo headerLocationLink">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png" alt="">
						</a>
						<a href="http://maps.google.com/?q=1 Miracle Mile Drive, Rochester, NY 14623" target="_blank" class="Rochester headerLocationLink">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png" alt="">
						</a>
						<a href="http://maps.google.com/?q=9090 Destiny USA Drive, Syracuse, NY 13204" target="_blank" class="Syracuse headerLocationLink">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/pointer.png" alt="">
						</a>
					</li>
				</ul>
			</div>
		</div>
	</section>
	<header class="clearfix">
	 	<nav class="navbar">
			<div class="container-fluid">
				<div class="navbar-header">
					<div class="tel mobil">
						<a href="tel:<?php //echo ot_get_option( 'phone_number' ); ?>">
							<i class="fa fa-phone"></i>
						</a>
						<a href="tel:201-333-7223" class="JerseyCity headerLocationLink">
							<i class="fa fa-phone"></i>
						</a>
						<a href="tel:631-752-7223" class="LongIsland headerLocationLink">
							<i class="fa fa-phone"></i>
						</a>
						<a href="tel:716-683-7223" class="Buffalo headerLocationLink">
							<i class="fa fa-phone"></i>
						</a>
						<a href="tel:585-427-7223" class="Rochester headerLocationLink">
							<i class="fa fa-phone"></i>
						</a>
						<a href="tel:315-423-7223" class="Syracuse headerLocationLink">
							<i class="fa fa-phone"></i>
						</a>
					</div>
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span style="margin-left: 5px;" class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<figure class="logo pull-left">
						<a href="/"><img class="logo-default" style="max-width: 151px; height: 51px;" src="<?php bloginfo('template_url'); ?>/assets/images/RPM-logo.svg" alt=""><img class="logo-sticky" style="max-width: 305px; height: 39px;" src="<?php bloginfo('template_url') ?>/assets/images/long-logo.svg"></a>
					</figure>
				</div>
				<ul class="nav hidden-xs navbar-nav navbar-right collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <?php
                        wp_nav_menu(array(
                            'theme_location' => 'main-menu',
                            'container'      => '',
                            'items_wrap'    => '%3$s'
                        ));
                     ?>
					<li class="arrive-btn"><a href="/race/race-pricing/" class="arrive"><img src="<?php bloginfo('template_url'); ?>/assets/images/staring.svg" alt=""> arrive and drive</a></li>
					<li class="search-btn"><a href="javascript:;"><i class="fa fa-search"></i></a></li>
					<div class="search-section" style="display: none;">
						<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="search" name="s" id="s" placeholder="Search...">
						</form>
					</div>
				</ul>
			</div>
		</nav>
	</header>

	<div class="menu-right">
	    <div class="close-right">
	        <i class="fa fa-close"></i>
	    </div>
	    <div class="location-heading clearfix" style="margin-top: 10px; margin-bottom: 30px;">
			<?php include 'template-part-location.php'; ?>
		</div>
	    <ul>
	        <?php
                wp_nav_menu(array(
                    'theme_location' => 'main-menu',
                    'container'      => '',
                    'items_wrap'    => '%3$s'
                ));
             ?>
			<li class="search-btn search-mob"><a href="javascript:;"><i class="fa fa-search"></i></a></li>
			<div class="search-section" style="display: none;">
				<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<input type="search" name="s" id="s" placeholder="Search...">
				</form>
			</div>
	    </ul>
	</div>