	<footer class="main-footer">
		<div class="container footer-top">
			<div class="row">
				<div class="col-sm-8">
					<img class="img-responsive" style="max-width: 431px;" src="<?php bloginfo('template_url') ?>/assets/images/long-logo.svg">
				</div>
				<div class="col-sm-4 footer-social">
					<?php if (ot_get_option( 'facebook_link' )) { ?>
						<a href="<?php echo ot_get_option( 'facebook_link' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
					<?php } ?>
					<?php if (ot_get_option( 'instagram_link' )) { ?>
						<a href="<?php echo ot_get_option( 'instagram_link' ); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
					<?php } ?>
					<?php if (ot_get_option( 'youtube_link' )) { ?>
						<a href="<?php echo ot_get_option( 'youtube_link' ); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
					<?php } ?>
					<?php if (ot_get_option( 'twitter_link' )) { ?>
						<a href="<?php echo ot_get_option( 'twitter_link' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
					<?php } ?>
				</div>
			</div>
		</div>
		<div class="container footer-widget">
			<div class="row">
<div id="text-2" class="widget-container col-sm-5 widget_text">
	<h3 class="widget-title">BE IN THE KNOW</h3>
	<div class="textwidget">
	<p>Keep up with our racing events, monthly promos and much more. You select the emails you want to receive. We will never share your information.</p>
	<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="Syracuse newsForm">
			<form action="//rpmraceway.us16.list-manage.com/subscribe/post?u=919b6550b77fcbae98303297d&amp;id=242972d0cb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-signup clearfix" target="_blank" novalidate>
					<div class="clearfix">
						<label for="mce-group[197]-197-0"><input type="checkbox" value="1" name="group[197][1]" id="mce-group[197]-197-0">Monthly Newsletter</label>
						<label for="mce-group[197]-197-1"><input type="checkbox" value="2" name="group[197][2]" id="mce-group[197]-197-1">Racing Leagues</label>
						<label for="mce-group[197]-197-2"><input type="checkbox" value="4" name="group[197][4]" id="mce-group[197]-197-2">For Parents</label>
						<label for="mce-group[197]-197-3"><input type="checkbox" value="8" name="group[197][8]" id="mce-group[197]-197-3">Racing Camps</label>
					</div>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your name" required="required">
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your best email address" required="required">
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
			    <div id="mc_embed_signup_scroll" style="display: none;">
					<div class="mc-field-group">
						<label for="mce-EMAIL">Email Address </label>
					</div>
					<div class="mc-field-group">
						<label for="mce-FNAME">First Name </label>
					</div>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none"></div>
						</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
					    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_919b6550b77fcbae98303297d_242972d0cb" tabindex="-1" value=""></div>
					    <div class="clear"></div>
				</div>
			</form>
		</div>
	<!--End mc_embed_signup-->
	<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="LongIsland newsForm">
			<form action="//rpmraceway.us16.list-manage.com/subscribe/post?u=919b6550b77fcbae98303297d&amp;id=623ecd86a9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-signup clearfix" target="_blank" novalidate>
					<div class="clearfix">
						<label for="mce-group[281]-281-0"><input type="checkbox" value="1" name="group[281][1]" id="mce-group[281]-281-0">Monthly Newsletter</label>
						<label for="mce-group[281]-281-1"><input type="checkbox" value="2" name="group[281][2]" id="mce-group[281]-281-1">Racing Leagues</label>
						<label for="mce-group[281]-281-2"><input type="checkbox" value="4" name="group[281][4]" id="mce-group[281]-281-2">For Parents</label>
						<label for="mce-group[281]-281-3"><input type="checkbox" value="8" name="group[281][8]" id="mce-group[281]-281-3">Racing Camps</label>
					</div>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your name" required="required">
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your best email address" required="required">
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
	    <div id="mc_embed_signup_scroll" style="display: none;">

		<div class="mc-field-group">
			<label for="mce-EMAIL">Email Address </label>
		</div>
		<div class="mc-field-group">
			<label for="mce-FNAME">First Name </label>
		</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_919b6550b77fcbae98303297d_623ecd86a9" tabindex="-1" value=""></div>
		    <div class="clear"></div>
		    </div>
		</form>
		</div>
	<!--End mc_embed_signup-->
	<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="JerseyCity newsForm">
		<form action="//rpmraceway.us16.list-manage.com/subscribe/post?u=919b6550b77fcbae98303297d&amp;id=c2439af3f8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-signup clearfix" target="_blank" novalidate>
					<div class="clearfix">
						<label for="mce-group[285]-285-0"><input type="checkbox" value="1" name="group[285][1]" id="mce-group[285]-285-0">Monthly Newsletter</label>
						<label for="mce-group[285]-285-1"><input type="checkbox" value="2" name="group[285][2]" id="mce-group[285]-285-1">Racing Leagues</label>
						<label for="mce-group[285]-285-2"><input type="checkbox" value="4" name="group[285][4]" id="mce-group[285]-285-2">For Parents</label>
						<label for="mce-group[285]-285-3"><input type="checkbox" value="8" name="group[285][8]" id="mce-group[285]-285-3">Racing Camps</label>
					</div>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your name" required="required">
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your best email address" required="required">
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
		    <div id="mc_embed_signup_scroll" style="display: none;">

		<div class="mc-field-group">
			<label for="mce-EMAIL">Email Address </label>
		</div>
		<div class="mc-field-group">
			<label for="mce-FNAME">First Name </label>
		</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_919b6550b77fcbae98303297d_c2439af3f8" tabindex="-1" value=""></div>
		    <div class="clear"></div>
		    </div>
		</form>
		</div>
		<!--End mc_embed_signup-->
		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="Rochester newsForm">
		<form action="//rpmraceway.us16.list-manage.com/subscribe/post?u=919b6550b77fcbae98303297d&amp;id=b221f787cb" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-signup clearfix" target="_blank" novalidate>
					<div class="clearfix">
						<label for="mce-group[289]-289-0"><input type="checkbox" value="1" name="group[289][1]" id="mce-group[289]-289-0">Monthly Newsletter</label>
						<label for="mce-group[289]-289-1"><input type="checkbox" value="2" name="group[289][2]" id="mce-group[289]-289-1">Racing Leagues</label>
						<label for="mce-group[289]-289-2"><input type="checkbox" value="4" name="group[289][4]" id="mce-group[289]-289-2">For Parents</label>
						<label for="mce-group[289]-289-3"><input type="checkbox" value="8" name="group[289][8]" id="mce-group[289]-289-3">Racing Camps</label>
					</div>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your name" required="required">
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your best email address" required="required">
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
		    <div id="mc_embed_signup_scroll" style="display: none;">

		<div class="mc-field-group">
			<label for="mce-EMAIL">Email Address </label>
		</div>
		<div class="mc-field-group">
			<label for="mce-FNAME">First Name </label>
		</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_919b6550b77fcbae98303297d_b221f787cb" tabindex="-1" value=""></div>
		    <div class="clear"></div>
		    </div>
		</form>
		</div>
		<!--End mc_embed_signup-->

		<!-- Begin MailChimp Signup Form -->
		<div id="mc_embed_signup" class="Buffalo newsForm">
		<form action="//rpmraceway.us16.list-manage.com/subscribe/post?u=919b6550b77fcbae98303297d&amp;id=0dd57dc956" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate newsletter-signup clearfix" target="_blank" novalidate>
					<div class="clearfix">
						<label for="mce-group[293]-293-0"><input type="checkbox" value="1" name="group[293][1]" id="mce-group[293]-293-0">Monthly Newsletter</label>
						<label for="mce-group[293]-293-1"><input type="checkbox" value="2" name="group[293][2]" id="mce-group[293]-293-1">Racing Leagues</label>
						<label for="mce-group[293]-293-2"><input type="checkbox" value="4" name="group[293][4]" id="mce-group[293]-293-2">For Parents</label>
						<label for="mce-group[293]-293-3"><input type="checkbox" value="8" name="group[293][8]" id="mce-group[293]-293-3">Racing Camps</label>
					</div>
					<input type="text" value="" name="FNAME" class="" id="mce-FNAME" placeholder="Your name" required="required">
					<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Your best email address" required="required">
					<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
		    <div id="mc_embed_signup_scroll" style="display: none;">

		<div class="mc-field-group">
			<label for="mce-EMAIL">Email Address </label>
		</div>
		<div class="mc-field-group">
			<label for="mce-FNAME">First Name </label>
		</div>
			<div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_919b6550b77fcbae98303297d_0dd57dc956" tabindex="-1" value=""></div>
		    <div class="clear"></div>
		    </div>
		</form>
		</div>

		<!--End mc_embed_signup-->
</div>
</div>
				<?php //dynamic_sidebar('footer-subscription-area'); ?>
				<div class="col-sm-1">
				</div>
				<?php dynamic_sidebar('footer-widget-area'); ?>
				<div class="widget-container col-sm-2 widget_nav_menu">
					<h3 class="widget-title">SET YOUR LOCATION</h3>
					<div class="menu-locations-menu-container locationStuff">
						<ul id="menu-locations-menu" class="menu">
							<li><a href="javascript:;" data-location=".JerseyCity">Jersey City</a></li>
							<li><a href="javascript:;" data-location=".LongIsland">Long Island</a></li>
							<li><a href="javascript:;" data-location=".Buffalo">Buffalo</a></li>
							<li><a href="javascript:;" data-location=".Rochester">Rochester</a></li>
							<li><a href="javascript:;" data-location=".Syracuse">Syracuse</a></li>
						</ul>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div class="row">
				<div class="col-sm-5">
					<h3>CONTACT US</h3>
					<a href="mailto:<?php echo ot_get_option( 'email_address' ); ?>"><i class="fa fa-envelope"></i> <?php echo ot_get_option( 'email_address' ); ?></a><br>
				<a href="tel:201-333-7223" class="JerseyCity headerLocationLink">
					<i class="fa fa-phone"></i> 201-333-7223
				</a>
				<a href="tel:631-752-7223" class="LongIsland headerLocationLink">
					<i class="fa fa-phone"></i> 631-752-7223
				</a>
				<a href="tel:716-683-7223" class="Buffalo headerLocationLink">
					<i class="fa fa-phone"></i> 716-683-7223
				</a>
				<a href="tel:585-427-7223" class="Rochester headerLocationLink">
					<i class="fa fa-phone"></i> 585-427-7223
				</a>
				<a href="tel:‎315-423-7223" class="Syracuse headerLocationLink">
					<i class="fa fa-phone"></i> ‎315-423-7223
				</a>

				</div>
				<div class="col-sm-1">
				</div>
				<?php dynamic_sidebar('stay-connected-embed-code'); ?>
			</div>
		</div>
		<div class="container footer-copyrights">
			<div class="row">
				<div class="col-sm-6">
					© <?php echo date('Y'); ?> RPM Raceway. All rights reserved.
				</div>
				<div class="col-sm-6">
					<ul>
	                    <?php
	                        wp_nav_menu(array(
	                            'theme_location' => 'copyrights-menu',
	                            'container'      => '',
	                            'items_wrap'    => '%3$s'
	                        ));
	                     ?>
					</ul>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.bxslider.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/owl.carousel.min.js"></script>
	<script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/masonry.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/aos.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/parallax.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
	<script type="text/javascript">
        var visitorLocation = jQuery.cookie('visitorLocation');
        jQuery('.locationHeaderStuffHeading').html(visitorLocation);
        var visitorLocationClass = jQuery.cookie('visitorLocationClass');

		jQuery(document).ready(function() {
		  jQuery('.collapse.in').prev('.panel-heading').addClass('active');
		  jQuery('#accordion, #bs-collapse')
		    .on('show.bs.collapse', function(a) {
		      jQuery(a.target).prev('.panel-heading').addClass('active');
		    })
		    .on('hide.bs.collapse', function(a) {
		      jQuery(a.target).prev('.panel-heading').removeClass('active');
		    });
		});
	</script>
	<script type="text/javascript">

        // switch contact RMP Raceway Form based on location
        function switchContactRPMForm(locationClass) {

            var formContainerSelector = ".contactForm",
                url, scriptElement,
                forms = {
                    '.Buffalo': 'https://api.tripleseat.com/v1/leads/ts_script.js?lead_form_id=2100&public_key=a903b24b2d06aaaf34ce2387c888dd29bbca26d1',
                    '.JerseyCity': 'https://api.tripleseat.com/v1/leads/ts_script.js?lead_form_id=209&public_key=a903b24b2d06aaaf34ce2387c888dd29bbca26d1',
                    '.LongIsland': 'https://api.tripleseat.com/v1/leads/ts_script.js?lead_form_id=2977&public_key=a903b24b2d06aaaf34ce2387c888dd29bbca26d1',
                    '.Rochester': 'https://api.tripleseat.com/v1/leads/ts_script.js?lead_form_id=4553&public_key=a903b24b2d06aaaf34ce2387c888dd29bbca26d1',
                    '.Syracuse': 'https://api.tripleseat.com/v1/leads/ts_script.js?lead_form_id=1371&public_key=a903b24b2d06aaaf34ce2387c888dd29bbca26d1',
                };
            url = forms[locationClass];
            if (url) {
                scriptElement  = document.createElement("script");
                scriptElement.type = "text/javascript";
                scriptElement.src = url;
                jQuery(formContainerSelector).each(function(){
                    jQuery(this).children().remove();
                });
                jQuery(formContainerSelector).hide();
                jQuery(formContainerSelector+formContainerSelector).show();
                //jQuery(locationClass+formContainerSelector).append(scriptElement);
                document.write("\x3Cscript src='"+url+"'>\x3C/script>");
            }
            
        }
        

	    jQuery(document).ready(function(){
            console.log(jQuery(visitorLocationClass+'.contactForm'));
            var $ = jQuery,
                pageName = '<?php global $pagename; echo $pagename; ?>',
                formLocationFieldMap = {
                    'book-a-racing-lesson' : ['#input_9_9'],
                    'careers' : ['#input_2_23', '#input_2_18'],
                    'contact' : ['#input_1_9', '#input_1_3'],
                    'email-us-photos-videos' : ['#input_4_6'],
                    'group-events-inquiries' : ['#input_3_13'],
                    'inquire-about-leagues' : ['#input_5_6'],
                    'register-for-summer-camps' : ['#input_8_6']
                };

			var visitorLocationToEmail = 'JC@rpmraceway.com';

			if (visitorLocation == 'Jersey City') {visitorLocationToEmail = 'JC@rpmraceway.com';};
			if (visitorLocation == 'Long Island') {visitorLocationToEmail = 'LI@rpmraceway.com';};
			if (visitorLocation == 'Rochester') {visitorLocationToEmail = 'ROCH@rpmraceway.com';};
			if (visitorLocation == 'Syracuse') {visitorLocationToEmail = 'SYR@rpmraceway.com';};
			if (visitorLocation == 'Buffalo') {visitorLocationToEmail = 'BUFF@rpmraceway.com';};

	    	// jQuery("<?php echo (isset($_COOKIE['visitorLocation'])) ? '.'.$_COOKIE['visitorLocation'] : 'body'; ?>").show();
	    	jQuery(visitorLocationClass+'.map-btn').attr('style', 'display: inline-block;');
	    	jQuery(visitorLocationClass+'.headerLocationLink').attr('style', 'display: inline-block;');
	    	jQuery(visitorLocationClass+'.playAndEatSec').attr('style', 'display: block;');
	    	jQuery(visitorLocationClass+'.mapIframe').attr('style', 'display: block;');
	    	//jQuery(visitorLocationClass+'.contactForm').attr('style', 'display: block;');
            if (pageName == 'contact-rpm-raceway') {
                switchContactRPMForm(visitorLocationClass);
            }
            
            
	    	jQuery(visitorLocationClass+'.calendarPage').attr('style', 'height: auto;');
	    	jQuery(visitorLocationClass+'.newsForm').attr('style', 'display: block;');

	    	jQuery(".priceAnnual").html(jQuery(visitorLocationClass+'.AnnualPrices').html());

			jQuery('.locationStuff ul li a').click(function(){

				jQuery('.locationHeaderStuffHeading').html(jQuery(this).html());
				jQuery('.locationStuff > h3').html(jQuery(this).html());
				var elm = jQuery(this),
                    thisValue = elm.text(),
                    currentContent = jQuery(this).attr('data-location'),
					cookieLocation = currentContent,
					contentFilter = currentContent;
				jQuery('.map-btn.JerseyCity, .map-btn.LongIsland, .map-btn.Buffalo, .map-btn.Rochester, .map-btn.Syracuse').hide();
				jQuery('.headerLocationLink.JerseyCity, .headerLocationLink.LongIsland, .headerLocationLink.Buffalo, .headerLocationLink.Rochester, .headerLocationLink.Syracuse').hide();
				jQuery('.playAndEatSec.JerseyCity, .playAndEatSec.LongIsland, .playAndEatSec.Buffalo, .playAndEatSec.Rochester, .playAndEatSec.Syracuse').hide();
				jQuery('.mapIframe.JerseyCity, .mapIframe.LongIsland, .mapIframe.Buffalo, .mapIframe.Rochester, .mapIframe.Syracuse').hide();
//				jQuery('.contactForm.JerseyCity, .contactForm.LongIsland, .contactForm.Buffalo, .contactForm.Rochester, .contactForm.Syracuse').hide();
				jQuery('.calendarPage.JerseyCity, .calendarPage.LongIsland, .calendarPage.Buffalo, .calendarPage.Rochester, .calendarPage.Syracuse').attr('style', 'height: 0px;');
				jQuery('.newsForm.JerseyCity, .newsForm.LongIsland, .newsForm.Buffalo, .newsForm.Rochester, .newsForm.Syracuse').hide();

				jQuery(".priceAnnual").html(jQuery(currentContent+"AnnualPrices").html());

				jQuery(currentContent+'.map-btn').attr('style', 'display: inline-block;');
				jQuery(currentContent+'.headerLocationLink').attr('style', 'display: inline-block;');
				jQuery(currentContent+'.playAndEatSec').attr('style', 'display: block;');
				jQuery(currentContent+'.mapIframe').attr('style', 'display: block;');
//				jQuery(currentContent+'.contactForm').attr('style', 'display: block;');
                if (pageName == 'contact-rpm-raceway') {
                    switchContactRPMForm(visitorLocationClass);
                }
				jQuery(currentContent+'.calendarPage').attr('style', 'height: auto;');
				jQuery(currentContent+'.newsForm').attr('style', 'display: block;');


				if (/JerseyCity/i.test(cookieLocation)) { cookieLocation = 'Jersey City';}
				if (/LongIsland/i.test(cookieLocation)) { cookieLocation = 'Long Island';}


				jQuery.removeCookie("visitorLocation");
				jQuery.removeCookie("visitorLocationClass");
				// updateLocationcookie = cookieLocation.replace(' ', '');
				jQuery.cookie("visitorLocation", thisValue, { expires : 1, path : '/' });
				jQuery.cookie("visitorLocationClass", currentContent, { expires : 1, path : '/' });


			    jQuery('.grid').isotope({
					itemSelector: '.grid-item',
					layout: 'masonry',
					filter: contentFilter
				});

			 	AOS.init({
					easing: 'ease-in-out-sine'
				});


				if (thisValue == 'Jersey City') {visitorLocationToEmail = 'JC@rpmraceway.com';};
				if (thisValue == 'Long Island') {visitorLocationToEmail = 'LI@rpmraceway.com';};
				if (thisValue == 'Rochester') {visitorLocationToEmail = 'ROCH@rpmraceway.com';};
				if (thisValue == 'Syracuse') {visitorLocationToEmail = 'SYR@rpmraceway.com';};
				if (thisValue == 'Buffalo') {visitorLocationToEmail = 'BUFF@rpmraceway.com';};

				jQuery('#input_9_11').val(visitorLocationToEmail);
				jQuery('#input_2_24').val(visitorLocationToEmail);
				jQuery('#input_1_10').val(visitorLocationToEmail);
				jQuery('#input_4_7').val(visitorLocationToEmail);
				jQuery('#input_3_14').val(visitorLocationToEmail);
				jQuery('#input_5_8').val(visitorLocationToEmail);
				jQuery('#input_8_8').val(visitorLocationToEmail);


                if (formLocationFieldMap[pageName]) {
                    jQuery.each(formLocationFieldMap[pageName], function (index, value){
                        jQuery(value).val(thisValue);
                    });
                }
			});

			// jQuery('.nav > li.menu-item-has-children a').click(function(){
			// 	jQuery(this).parent().find('.sub-menu').toggle();
			// 	jQuery(this).parent().toggleClass('showArrow');


			// 	jQuery('body').click(function(){
			// 		jQuery(this).parent().find('.sub-menu').hide();
			// 		jQuery(this).find('.nav > li.menu-item-has-children').removeClass('showArrow');
			// 	});

			// 	return false;
			// });


			<?php if (is_page('book-a-racing-lesson')) { ?>
				jQuery('#input_9_9').val(visitorLocation);
				jQuery('#input_9_11').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('careers')) { ?>
				jQuery('#input_2_23').val(visitorLocation);
				jQuery('#input_2_18').val(visitorLocation);
				jQuery('#input_2_24').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('contact')) { ?>
				jQuery('#input_1_9').val(visitorLocation);
				jQuery('#input_1_3 option[value="'+visitorLocation+'"]').attr('selected', 'selected');
				jQuery('#input_1_10').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('email-us-photos-videos')) { ?>
				jQuery('#input_4_6').val(visitorLocation);
				jQuery('#input_4_7').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('group-events-inquiries')) { ?>
				jQuery('#input_3_13').val(visitorLocation);
				jQuery('#input_3_14').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('inquire-about-leagues')) { ?>
				jQuery('#input_5_6').val(visitorLocation);
				jQuery('#input_5_8').val(visitorLocationToEmail);
			<?php } ?>

			<?php if (is_page('register-for-summer-camps')) { ?>
				jQuery('#input_8_6').val(visitorLocation);
				jQuery('#input_8_8').val(visitorLocationToEmail);
			<?php } ?>

	    	jQuery('.loop').owlCarousel({
			    center: false,
			    items:1,
			    loop:false,
			    margin:40,
			    autoplay:false,
			    responsive:{
			       769:{
			    		center: true,
			    		margin:80,
			    		autoplay:true,
			            items:4
			        }
			    }
			});

			jQuery(".search-btn a").click(function(){
			    jQuery(".search-section").fadeToggle();
			    jQuery(".search-btn a i").toggleClass('fa-remove');
			});

			jQuery('.banner-slider').bxSlider({
			  auto: true,
			  pager: false,
			  controls: false,
			  speed: 800,
			  autoHover: true
			});

			 jQuery(window).scroll(function() {
		        var scroll = jQuery(window).scrollTop();
		        if (scroll >= 50) {
		            jQuery("header").addClass("sticky");
		        } else {
		            jQuery("header").removeClass("sticky");
		        }
		    });

			 jQuery(".full-width").owlCarousel({
	            items: 1,
	            singleItem: true,
	            autoHeight: false
	        });
			 jQuery('.review').owlCarousel({
			    items:1,
			    loop:true,
			    margin:100,
			    autoplay:true,
				autoplayTimeout:5000,
				autoplayHoverPause:true,
			    responsive:{
			        768:{
			            items:3
			        }
			    }
			});

			jQuery(".arcade").owlCarousel({
	            items: 1,
	            singleItem: true
	        });

	        jQuery('.grid').isotope({
			// options
				itemSelector: '.grid-item',
				layout: 'masonry',
				filter: visitorLocationClass
			});

		    jQuery('.map-slider').bxSlider({
		    	auto: true,
				pagerCustom: '#bx-pager',
				margin: 20
			});

			jQuery('.partner').bxSlider({
			  minSlides: 2,
			  maxSlides: 4,
			  slideWidth: 221,
			  slideMargin: 100,
			  moveSlides: 1,
			  controlls: false,
			  pager: false
			});

			jQuery('.navbar-toggle').on( "click", function() {
				jQuery(this).parents('body').find('.menu-right').css('position', 'absolute').animate({right: "0"});
				jQuery('.menu-right').height(jQuery('body').height());
			});

			jQuery('.menu-right .close-right i').on( "click", function() {
				jQuery(this).parents('body').find('.menu-right').css('position', 'fixed').animate({right: "-100%"});
			});

		    var htb = jQuery('body').height();
			jQuery('.menu-right').height(htb);


			// jQuery('.locationHeaderStuff ul li a').click(function(){
			// 	jQuery('.locationStuff > h3').html(jQuery(this).html());
			// 	jQuery('.locationHeaderStuffHeading').html(jQuery(this).html());
			// 	var currentContent = jQuery(this).attr('data-location');
			// 	jQuery('.JerseyCity, .LongIsland, .Buffalo, .Rochester, .Syracuse').hide();
			// 	jQuery(currentContent).show();


			//     jQuery('.grid').isotope({
			// 	// options
			// 		itemSelector: '.grid-item',
			// 		layout: 'masonry',
			// 		filter: visitorLocationClass
			// 	});

			//  	AOS.init({
			// 		easing: 'ease-in-out-sine'
			// 	});
			// });

			jQuery("*[data-aos]").each(function(){
			    jQuery(this).attr('data-aos-once', 'true');
			});

		});

		jQuery(window).resize(function(){
			var htb = jQuery('body').height();
			jQuery('.menu-right').height(htb);
		});
	 	AOS.init({
			easing: 'ease-in-out-sine'
		});
	</script>
	<script type="text/javascript">
		jQuery('a[data-toggle=tab]').each(function () {
		  var $this = jQuery(this);

		  $this.on('shown.bs.tab', function () {
		    jQuery('.grid').isotope({
			// options
				itemSelector: '.grid-item',
				layout: 'masonry',
				filter: visitorLocationClass
			});
		  });
		});
	</script>
	<script type="text/javascript">
		// jQuery(function ($) {
		//     var validator = jQuery('#news_action').validate({
		//         rules: {
		//             name: {
		//                 required: true
		//             },
		//             email: {
		//                 required: true,
		//                 email: true
		//             }
		//         },
		//         messages: {},
		//         highlight: function (element) {
		//             jQuery(element).parent().addClass('error')
		//         },
		//         unhighlight: function (element) {
		//             jQuery(element).parent().removeClass('error')
		//         }
		//     });
		// });
		jQuery('[data-parallax="scroll"]').each(function() {
		    var $this = jQuery(this);
		    $this.parallax({imageSrc: $this.data('image-src') });
		});
	</script>
	<?php wp_footer(); ?>
	<script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '838778449603710'); // Insert your pixel ID here.fbq('track', 'PageView');

    </script><noscript><img height="1" width="1" style="display:none"src="https://www.facebook.com/tr?id=838778449603710&ev=PageView&noscript=1"/></noscript>
<script src="https://pix.a8zv.net/rpm/rpm_pixel.js"></script>
</body>
</html>