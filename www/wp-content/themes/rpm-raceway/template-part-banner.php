<?php
	$bannerImage = (get_field('banner_image')) ? 'background-image: url('.get_field('banner_image').');' : 'background-image: url(/wp-content/uploads/2017/06/generic-header-3.jpg);';
	$bannerText = (get_field('banner_text')) ? get_field('banner_text') : '';
	if (basename(get_page_template()) === 'page.php') {
		$bannerText = (get_field('banner_text')) ? get_field('banner_text') : get_the_title();
	}
	if (is_singular('press-room')) {
		$bannerImage = 'background-image: url('.get_the_post_thumbnail_url().');';
	}
?>
<section class="sub-banner" style="position: relative; <?php echo $bannerImage; ?>;">
	<div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-12" data-aos="zoom-in-up" data-aos-duration="1000">
				<?php if ($bannerText != '') { ?>
					<h2><?php echo $bannerText; ?></h2>
				<?php } ?>
			</div>
		</div>
	</div>
</section>