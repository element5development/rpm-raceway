<?php
add_action( 'after_setup_theme', 'rpm_setup' );
function rpm_setup() {
	load_theme_textdomain( 'cfwr', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 640;
	register_nav_menus(
		array(
      'main-menu' => __( 'Main Menu', 'cfwr' ),
      'copyrights-menu' => __( 'Copyrights Menu', 'cfwr' )
    )
	);
}

add_action( 'wp_enqueue_scripts', 'rpm_load_scripts' );
function rpm_load_scripts() {
	wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'rpm-raceway-custom', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ), '1.0.0', true );
}

add_action( 'comment_form_before', 'rpm_enqueue_comment_reply_script' );
function rpm_enqueue_comment_reply_script() {
	if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}

add_filter( 'wp_title', 'rpm_filter_wp_title' );
function rpm_filter_wp_title( $title ) {
	return $title . esc_attr( get_bloginfo( 'name' ) );
}

add_action( 'widgets_init', 'rpm_widgets_init' );
function rpm_widgets_init() {
  register_sidebar( array (
    'name' => __( 'Sidebar Widget Area', 'cfwr' ),
    'id' => 'primary-widget-area',
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => "</li>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );
  register_sidebar( array (
    'name' => __( 'Footer Subscription Area', 'cfwr' ),
    'id' => 'footer-subscription-area',
    'before_widget' => '<div id="%1$s" class="widget-container col-sm-5 %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );
  register_sidebar( array (
    'name' => __( 'Footer Widget Area', 'cfwr' ),
    'id' => 'footer-widget-area',
    'before_widget' => '<div id="%1$s" class="widget-container col-sm-2 %2$s">',
    'after_widget' => "</div>",
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ) );
	register_sidebar( array (
		'name' => __( 'Stay Connected Embed Code', 'cfwr' ),
		'id' => 'stay-connected-embed-code',
		'before_widget' => '<div id="%1$s" class="widget-container col-sm-6 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}

function rpm_custom_post_type() {
    $labels = array(
        'name'                => _x( 'Reviews', 'Post Type General Name', 'rpm' ),
        'singular_name'       => _x( 'Review', 'Post Type Singular Name', 'rpm' ),
        'menu_name'           => __( 'Reviews', 'rpm' ),
        'parent_item_colon'   => __( 'Parent Review', 'rpm' ),
        'all_items'           => __( 'All Reviews', 'rpm' ),
        'view_item'           => __( 'View Review', 'rpm' ),
        'add_new_item'        => __( 'Add New Review', 'rpm' ),
        'add_new'             => __( 'Add New', 'rpm' ),
        'edit_item'           => __( 'Edit Review', 'rpm' ),
        'update_item'         => __( 'Update Review', 'rpm' ),
        'search_items'        => __( 'Search Review', 'rpm' ),
        'not_found'           => __( 'Not Found', 'rpm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'rpm' ),
    );

    $args = array(
        'label'               => __( 'Reviews', 'rpm' ),
        'description'         => __( 'Reviews', 'rpm' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array('title', 'editor'),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'rpm_reviews',
    'capabilities'        => array(
      'publish_posts'       => 'publish_rpm_reviews',
      'edit_posts'          => 'edit_rpm_reviews',
      'edit_others_posts'   => 'edit_others_rpm_reviews',
      'delete_posts'        => 'delete_rpm_reviews',
      'delete_others_posts' => 'delete_others_rpm_reviews',
      'read_private_posts'  => 'read_private_rpm_reviews',
      'edit_post'           => 'edit_rpm_review',
      'delete_post'         => 'delete_rpm_review',
      'read_post'           => 'read_rpm_review',
    )
    );
    register_post_type( 'rpm_reviews', $args );
}
add_action( 'init', 'rpm_custom_post_type', 0 );

function rpm_custom_post_type2() {
    $labels = array(
        'name'                => _x( 'Current Specials', 'Post Type General Name', 'rpm' ),
        'singular_name'       => _x( 'Current Special', 'Post Type Singular Name', 'rpm' ),
        'menu_name'           => __( 'Current Specials', 'rpm' ),
        'parent_item_colon'   => __( 'Parent Current Special', 'rpm' ),
        'all_items'           => __( 'All Current Specials', 'rpm' ),
        'view_item'           => __( 'View Current Special', 'rpm' ),
        'add_new_item'        => __( 'Add New Current Special', 'rpm' ),
        'add_new'             => __( 'Add New', 'rpm' ),
        'edit_item'           => __( 'Edit Current Special', 'rpm' ),
        'update_item'         => __( 'Update Current Special', 'rpm' ),
        'search_items'        => __( 'Search Current Special', 'rpm' ),
        'not_found'           => __( 'Not Found', 'rpm' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'rpm' ),
    );

    $args = array(
        'label'               => __( 'Current Specials', 'rpm' ),
        'description'         => __( 'Current Specials', 'rpm' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array('title', 'thumbnail', 'editor', 'excerpt'),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'rpm_current_specials',
    'capabilities'        => array(
      'publish_posts'       => 'publish_rpm_current_specials',
      'edit_posts'          => 'edit_rpm_current_specials',
      'edit_others_posts'   => 'edit_others_rpm_current_specials',
      'delete_posts'        => 'delete_rpm_current_specials',
      'delete_others_posts' => 'delete_others_rpm_current_specials',
      'read_private_posts'  => 'read_private_rpm_current_specials',
      'edit_post'           => 'edit_rpm_current_special',
      'delete_post'         => 'delete_rpm_current_special',
      'read_post'           => 'read_rpm_current_special',
    )
    );
    register_post_type( 'rpm_current_specials', $args );
}
add_action( 'init', 'rpm_custom_post_type2', 0 );



function pr_custom_post_type() {
    $labels = array(
        'name'                => _x( 'Press Room', 'Post Type General Name', 'twentysixteen' ),
        'singular_name'       => _x( 'Press Room', 'Post Type Singular Name', 'twentysixteen' ),
        'menu_name'           => __( 'Press Room', 'twentysixteen' ),
        'parent_item_colon'   => __( 'Parent Press Room', 'twentysixteen' ),
        'all_items'           => __( 'All Press Room', 'twentysixteen' ),
        'view_item'           => __( 'View Press Room', 'twentysixteen' ),
        'add_new_item'        => __( 'Add New Press Room', 'twentysixteen' ),
        'add_new'             => __( 'Add New', 'twentysixteen' ),
        'edit_item'           => __( 'Edit Press Room', 'twentysixteen' ),
        'update_item'         => __( 'Update Press Room', 'twentysixteen' ),
        'search_items'        => __( 'Search Press Room', 'twentysixteen' ),
        'not_found'           => __( 'Not Found', 'twentysixteen' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'twentysixteen' ),
    );

    $args = array(
        'label'               => __( 'Press Room', 'twentysixteen' ),
        'description'         => __( 'Press Room news and reviews', 'twentysixteen' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'custom-fields', 'thumbnail' ),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies'          => array( 'categories' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'press-room', $args );
}
add_action( 'init', 'pr_custom_post_type', 0 );

  // STYLEGUIDE HEX TO RGB
    function hex2rgb( $colour ) {
      if ( $colour[0] == '#' ) { $colour = substr( $colour, 1 ); }
      if ( strlen( $colour ) == 6 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
      } elseif ( strlen( $colour ) == 3 ) {
        list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
      } else {
        return false;
      }
      $r = hexdec( $r ); $g = hexdec( $g ); $b = hexdec( $b );
      return 'rgb('.$r.', '.$g.', '.$b.')';
    }
  // STYLEGUIDE RGB TO CMYK
    function hex2rgb2($hex) {
      $color = str_replace('#','',$hex);
      $rgb = array(
        'r' => hexdec(substr($color,0,2)),
        'g' => hexdec(substr($color,2,2)),
        'b' => hexdec(substr($color,4,2)),
      );
      return $rgb;
    }
    function rgb2cmyk($var1,$g=0,$b=0) {
      if (is_array($var1)) { $r = $var1['r']; $g = $var1['g']; $b = $var1['b'];
      } else { $r = $var1; }
      $r = $r / 255; $g = $g / 255; $b = $b / 255;
      $bl = 1 - max(array($r,$g,$b));
      if ( ( 1 - $bl ) > 0 ) {
        $c = ( 1 - $r - $bl ) / ( 1 - $bl );
        $m = ( 1 - $g - $bl ) / ( 1 - $bl );
        $y = ( 1 - $b - $bl ) / ( 1 - $bl );
      }
      $c = round($c * 100); $m = round($m * 100); $y = round($y * 100); $bl = round($bl * 100);
      return 'cmyk('.$c.', '.$m.', '.$y.', '.$bl.')';
    }


function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}


function distance($lat1, $lon1, $lat2, $lon2, $unit) {

  $theta = $lon1 - $lon2;
  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
  $dist = acos($dist);
  $dist = rad2deg($dist);
  $miles = $dist * 60 * 1.1515;
  $unit = strtoupper($unit);

  if ($unit == "K") {
    return ($miles * 1.609344);
  } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
        return $miles;
      }
}

function button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'class' => ''
  ), $atts, 'button');

  $button_shortcode = '<a href="'.$atts['link'].'" class="button '.$atts['class'].' '.$atts['style'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $button_shortcode = '<a href="'.$atts['link'].'" class="white-btn '.$atts['class'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $button_shortcode = '<a href="'.$atts['link'].'" class="red-btn '.$atts['class'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $button_shortcode = '<a href="'.$atts['link'].'" class="black-btn '.$atts['class'].'">'.$atts['text'].'</a>';
  }
  return $button_shortcode;
}
add_shortcode( 'button', 'button_shortcode' );

function jersey_city_button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'target' => '',
    'class' => ''
  ), $atts, 'button');

  $jersey_city_button_shortcode = '<a href="'.$atts['link'].'" class="button headerLocationLink JerseyCity '.$atts['style'].'" target="'.$atts['target'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $jersey_city_button_shortcode = '<a href="'.$atts['link'].'" class="white-btn headerLocationLink JerseyCity" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $jersey_city_button_shortcode = '<a href="'.$atts['link'].'" class="red-btn headerLocationLink JerseyCity" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $jersey_city_button_shortcode = '<a href="'.$atts['link'].'" class="black-btn headerLocationLink JerseyCity" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  return $jersey_city_button_shortcode;
}
add_shortcode( 'jersey-city-button', 'jersey_city_button_shortcode' );

function long_island_button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'target' => '',
    'class' => ''
  ), $atts, 'button');

  $long_island_button_shortcode = '<a href="'.$atts['link'].'" class="button headerLocationLink LongIsland '.$atts['style'].'" target="'.$atts['target'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $long_island_button_shortcode = '<a href="'.$atts['link'].'" class="white-btn headerLocationLink LongIsland" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $long_island_button_shortcode = '<a href="'.$atts['link'].'" class="red-btn headerLocationLink LongIsland" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $long_island_button_shortcode = '<a href="'.$atts['link'].'" class="black-btn headerLocationLink LongIsland" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  return $long_island_button_shortcode;
}
add_shortcode( 'long-island-button', 'long_island_button_shortcode' );

function buffalo_button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'target' => '',
    'class' => ''
  ), $atts, 'button');

  $buffalo_button_shortcode = '<a href="'.$atts['link'].'" class="button headerLocationLink Buffalo '.$atts['style'].'" target="'.$atts['target'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $buffalo_button_shortcode = '<a href="'.$atts['link'].'" class="white-btn headerLocationLink Buffalo" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $buffalo_button_shortcode = '<a href="'.$atts['link'].'" class="red-btn headerLocationLink Buffalo" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $buffalo_button_shortcode = '<a href="'.$atts['link'].'" class="black-btn headerLocationLink Buffalo" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  return $buffalo_button_shortcode;
}
add_shortcode( 'buffalo-button', 'buffalo_button_shortcode' );

function rochester_button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'target' => '',
    'class' => ''
  ), $atts, 'button');

  $rochester_button_shortcode = '<a href="'.$atts['link'].'" class="button headerLocationLink Rochester '.$atts['style'].'" target="'.$atts['target'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $rochester_button_shortcode = '<a href="'.$atts['link'].'" class="white-btn headerLocationLink Rochester" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $rochester_button_shortcode = '<a href="'.$atts['link'].'" class="red-btn headerLocationLink Rochester" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $rochester_button_shortcode = '<a href="'.$atts['link'].'" class="black-btn headerLocationLink Rochester" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  return $rochester_button_shortcode;
}
add_shortcode( 'rochester-button', 'rochester_button_shortcode' );

function syracuse_button_shortcode( $atts ) {
  $atts = shortcode_atts( array(
    'text' => 'inquire now',
    'link' => '#',
    'style' => 'red',
    'target' => '',
    'class' => ''
  ), $atts, 'button');

  $syracuse_button_shortcode = '<a href="'.$atts['link'].'" class="button headerLocationLink Syracuse '.$atts['style'].'" target="'.$atts['target'].'"><span>'.$atts['text'].'</span></a>';
  if ($atts['style'] == 'normal-red') {
    $syracuse_button_shortcode = '<a href="'.$atts['link'].'" class="white-btn headerLocationLink Syracuse" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-red2') {
    $syracuse_button_shortcode = '<a href="'.$atts['link'].'" class="red-btn headerLocationLink Syracuse" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  if ($atts['style'] == 'normal-black') {
    $syracuse_button_shortcode = '<a href="'.$atts['link'].'" class="black-btn headerLocationLink Syracuse" target="'.$atts['target'].'">'.$atts['text'].'</a>';
  }
  return $syracuse_button_shortcode;
}
add_shortcode( 'syracuse-button', 'syracuse_button_shortcode' );

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
  function loop_columns() {
    return 3; // 3 products per row
  }
}

// add_action('wp_enqueue_scripts', 'override_woo_frontend_styles');
function override_woo_frontend_styles(){
    $file_general = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/rpm-raceway/assets/css/woocustom.css';
    if( file_exists($file_general) ){
        wp_dequeue_style('woocommerce-layout');
        wp_enqueue_style('woocommerce-layout-custom', get_template_directory_uri() . '/assets/css/woocustom.css');
        wp_dequeue_style('woocommerce-general');
        wp_enqueue_style('woocommerce-general-custom', get_template_directory_uri() . '/assets/css/woogeneral.css');
        wp_dequeue_style('woocommerce-smallscreen');
        wp_enqueue_style('woocommerce-smallscreen-custom', get_template_directory_uri() . '/assets/css/woosmall.css');
    }
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  // $cols contains the current number of products per page based on the value stored on Options -> Reading
  // Return the number of products you wanna show per page.
  $cols = 9;
  return $cols;
}

function current_location_ajax() {
    $visitorLocation = json_decode(file_get_contents('http://freegeoip.net/json/'.getUserIP()));
    global $wpdb;
    $results = $wpdb->get_results( 'SELECT * FROM wp_rpm_locations', OBJECT );
    $distance = array();
    foreach ($results as $key => $value) {
    $distance[$key] = array(distance($visitorLocation->latitude, $visitorLocation->longitude, $value->lat, $value->long, "M") => $value->name);
    }
    $currentLocation = implode(" ", min($distance));
    echo 'Select Location';
    die;
}
add_action('wp_ajax_current_location_ajax', 'current_location_ajax');
add_action('wp_ajax_nopriv_current_location_ajax', 'current_location_ajax');

function add_location_column_press_room($columns) {
  unset($columns['date']);

  $columns = array_merge( $columns,
              array('location' => __('Location')) );

  $columns = array_merge( $columns,
              array('date' => __('Date')) );

    return $columns;
}
add_filter('manage_press-room_posts_columns' , 'add_location_column_press_room');

add_action( 'manage_posts_custom_column', 'add_location_col_data', 10, 2);
function add_location_col_data( $column_name, $post_id ) {

    if( $column_name == 'location' ) {
        $location = get_post_meta( $post_id, 'location', true );
        echo $location;
    }

}
add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}
// remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
  $args['posts_per_page'] = 3; // 4 related products
  return $args;
}

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );
function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


add_action ('save_post','conotes_wpseo_category_title',10,2);

function conotes_wpseo_category_title($post_ID, $post) {
    $meta   = get_option( 'wpseo_taxonomy_meta' , false);
    $categories=get_the_category($post_ID);
    $category_id = $categories[0]->cat_ID; // получаем id рубрики для записи
    $cattitle  = $meta['category'][$category_id]['wpseo_title']; // получаем title родительской рубрики
    update_post_meta($post_ID, 'cf__my_category_title', $_COOKIE['visitorLocation']); // сохраняем значение произвольного поля 
}
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );
function rpm_style_scripts() {
    wp_enqueue_style( 'style-rpm-css', get_stylesheet_uri().'?v='.strtotime(" now ") );
}
add_action( 'wp_enqueue_scripts', 'rpm_style_scripts' );

// add_filter('autoptimize_filter_css_replacetag','te_css_replacetag',10,1);
// function te_css_replacetag($replacetag) {
//   return array("</body>","before");
//   }

  add_filter('autoptimize_css_do_minify','my_ao_css_minify',10,1);
/**
 * Do we want to minify?
 * If set to false autoptimize effectively only aggregates, but does not minify.
 *
 * @return: boolean true or false
 */
function my_ao_css_minify() {
   return false;
}

add_filter( 'clean_url', function( $url )
{
  if (!is_page('careers')) {
    if ( FALSE === strpos( $url, 'jquery-migrate.min.js' ) && FALSE === strpos($url, 'wp-embed.min.js') )
    { // not our file
        return $url;
    }
    // Must be a ', not "!
    return "$url' async='async";
  } else {
    return $url;
  }
}, 11, 1 );
function load_custom_scripts() {
  if (!is_page('careers')) {
    wp_deregister_script( 'jquery' );
    // wp_register_script('jquery', '//code.jquery.com/jquery-2.2.4.min.js', array(), '2.2.4', true); // true will place script in the footer
    // wp_enqueue_script( 'jquery' );
  }
}
/*if(!is_admin()) {
    add_action('wp_enqueue_scripts', 'load_custom_scripts', 99);
}*/