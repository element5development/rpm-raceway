<?php get_header(); ?>

	<section class="sub-banner" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/blog.png'); position: relative;">
		<div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>PRESS ROOM</h2>
				</div>
			</div>
		</div>
	</section>

	<section class="white-bg">
		<div class="container text-center blog">

		</div>
	</section>

	<section class="white-bg blog-section">
		<div class="container">
			<div class="row">
				<div class="location-heading clearfix" style="margin-top: 20px; margin-bottom: 20px; max-width: 100%;">
					<a id="pressAll" href="javascript:;" style="position: absolute; right: 0px; top: 50px; font-size: 14px; color: #000;">Show All</a>
					<?php include 'template-part-location.php'; ?>
				</div>
			</div>
			<br>
			<div class="row one-blog-section">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php if (has_post_thumbnail()) { ?>
						<div class="clearfix <?php echo 'press-re '.get_field('location'); ?>">
						<div class="col-md-6" data-aos="fade-in-up" data-aos-duration="1000">
							<a href="<?php the_permalink(); ?>">
								<figure style="background: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-position: center;">
								</figure>
							</a>
						</div>
						<div data-aos="fade-in-up" data-aos-duration="1000" id="post-<?php the_ID(); ?>" <?php post_class('col-md-6 single-post-blog text-left press-re '.get_field('location')); ?>>
					<?php } else { ?>
						<div id="post-<?php the_ID(); ?>" <?php post_class('col-md-12 single-post-blog text-left press-re '.get_field('location')); ?>>
					<?php } ?>
				            <h2><?php the_category(', '); ?></h2>
				            <h1 class="header__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				            <?php the_excerpt(); ?>
				            <a href="<?php the_permalink(); ?>" class="black-btn">read the rest</a>
				        </div>
				       </div>
			    <?php endwhile; endif; ?>

			</div>
		</div>
	</section>
	<style type="text/css">.press-re {margin-bottom: 50px;}</style>
<script type="text/javascript">
	jQuery(function($){
		$('.locationStuff h3').html('ALL');
		$('#pressAll').click(function(){
			$('.press-re').attr('style', 'display: block;');
			$('.locationStuff h3').html('ALL');
		});
	});
</script>
	<?php include 'template-part-bottom-nav.php'; ?>
<?php get_footer(); ?>