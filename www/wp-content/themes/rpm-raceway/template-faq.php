 <?php
 /* Template Name: FAQ */
 get_header(); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="rules">
			<div class="container">
				<div <?php post_class('text'); ?>>
					<?php the_content() ?>
				</div>
				<?php if( have_rows('frequently_ask_questions') ) { ?>
				<div <?php post_class('text'); ?>>
					<div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
						<?php $i=0; while( have_rows('frequently_ask_questions') ) { the_row(); ?>
						<div class="panel">
							<div class="panel-heading" role="tab" id="heading<?php echo $i; ?>">
								<h4 class="panel-title">
									<a class="collapsed" role="button" style="font-weight: normal; font-size: 18px;" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
										Q: <?php the_sub_field('question'); ?>
									</a>
								</h4>
							</div>
							<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $i; ?>">
								<div class="panel-body">
									<strong>A:</strong> <?php the_sub_field('answer'); ?>
								</div>
							</div>
						</div>
						<?php $i++; } ?>
					</div>
				</div>
				<?php } ?>
			</div>
		</section>
    <?php endwhile; endif; ?>
<?php get_footer(); ?>