<?php get_header(); ?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="rules">
			<div class="container">
				<div id="post-<?php the_ID(); ?>" <?php post_class('text'); ?>>
					<?php if (has_post_thumbnail()) { ?>
					<img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive" style="width: 100%; max-width: 100%;">
					<br>
					<?php } ?>
            		<?php the_content(); ?>
				</div>
			</div>
		</section>
    <?php endwhile; endif; ?>
	<?php include 'template-part-bottom-nav.php'; ?>
<?php get_footer(); ?>