<?php get_header(); ?>
<section class="sub-banner" style="background-image: url('<?php bloginfo('template_url'); ?>/assets/images/generic-header-3.jpg'); position: relative;">
    <div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php single_cat_title(); ?></h2>
            </div>
        </div>
    </div>
</section>
<section class="white-bg">
    <div class="container text-center blog"></div>
</section>
<section class="white-bg blog-section">
    <div class="container">
        <div class="row">
            <div class="location-heading clearfix" style="max-width:100%;margin-top: 20px;">
                <div class="pull-left">
                    <h3>Select your category</h3>
                </div>
                <div class="pull-right">
                   <h3>Categories</h3>
                   <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
                    <ul class="dropdown-menu">
                       <li class="cat-item">
                           <a href="https://rpmraceway.com/categories/press-room/">All</a>
                       </li>
                        <?php wp_list_categories( array( 'child_of'=> '37','orderby'=> 'name', 'show_count'=> false, 'hide_empty'=> 0, 'title_li'=> '' ) ); ?> </ul>
                </div>
            </div>
        </div>
        <div class="row one-blog-section">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="clearfix" style="margin-bottom: 50px;">
                <?php if (has_post_thumbnail()){?>
                <div class="col-md-6" data-aos="fade-in-up" data-aos-duration="1000"><figure style="background: url(<?php the_post_thumbnail_url(); ?>); background-size: cover; background-position: center;"></figure></div>
                <div data-aos="fade-in-up" data-aos-duration="1000" id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-6 single-post-blog text-left'); ?>>
                    <?php }else{?>
                <div class="col-md-6" data-aos="fade-in-up" data-aos-duration="1000"><a href="<?php the_permalink(); ?>"><figure style="background: url(<?php  bloginfo('template_url'); ?>/assets/images/download.jpeg); background-size: cover; background-position: center;"></figure></a></div>
                <div data-aos="fade-in-up" data-aos-duration="1000" id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-6 single-post-blog text-left'); ?>>                    
                    <!--<div id="post-<?php the_ID(); ?>" <?php post_class( 'col-md-12 single-post-blog text-left'); ?>>-->
                        <?php }?>
                        <h1 class="header__title">
                            
                                <?php the_title(); ?>

                        </h1>
                        <h2>
                            <?php
                            $category = get_the_category();
                            $leng = count($category);
                            for($i = 0; $i < $leng; $i++) {
                                if ($category[$i]->cat_name == 'Print' || $category[$i]->cat_name == 'Online' || $category[$i]->cat_name == 'Television' || $category[$i]->cat_name == 'Radio') {
                                    $cat_name = $category[$i]->cat_name;
                                } else {
                                   $cat_name =  $category[0]->cat_name;
                                }
                            };
                            
                            if($cat_name == 'Categories') {
                                $cat_name = 'No category';
                            }  
                            echo $cat_name;
                            ?>
                        </h2>
                        <?php
                        
                        $st = '';
                        $content = get_the_content();
                        if(strlen($content) > 250) {
                            $st = '...';
                        };
                           $content = substr($content, 0, 250);
                           echo "<p>".$content.$st."</p>"; 
                    
                            $link = get_post_meta(get_the_ID(),'post_link');

                           echo '<a target="_blank" rel="nofollow" href="'.$link[0].'" class="black-btn">read the rest</a>';
                        ?> </div>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php include 'template-part-bottom-nav.php'; ?>
<?php get_footer(); ?>
<script>

    jQuery('.location-heading .pull-right h3').text(jQuery('.sub-banner h2').text());
    if(jQuery('.sub-banner h2').text() == 'Press Room') {
       jQuery('.location-heading .pull-right h3').text('All'); 
    }
</script>
