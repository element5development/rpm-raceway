<?php
/* Template Name: Eat & Drink */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/gaming-icon.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>
		<section class="white-bg play-section">
			<div class="container text-center">
				<div class="row">
					<div class="location-heading clearfix" style="margin-top: 50px;" >
						<?php include 'template-part-location.php'; ?>
					</div>
				</div>

				<?php if( have_rows('image_sliders_with_heading_&_content') ) { ?>
					<?php while( have_rows('image_sliders_with_heading_&_content') ) { the_row(); ?>
						<div class="row playAndEatSec <?php the_sub_field('location'); ?>">
							<h4><?php the_sub_field('title'); ?></h4>
							<figure class="line-break">
								<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
							</figure>
							<?php if (get_sub_field('content')) { ?>
								<p><?php the_sub_field('content'); ?></p>
							<?php } ?>
						<?php if( have_rows('images') ) { ?>
							<div class="arcade owl-carousel owl-theme">
								<?php while( have_rows('images') ) { the_row(); ?>
						            <div class="item">
						              	<figure><img src="<?php the_sub_field('image'); ?>"></figure>
						            </div>
								<?php } ?>
					      	</div>
						<?php } ?>
						</div>
					<?php } ?>
				<?php } ?>

			</div>
		</section>

		<?php if (get_field('parallax_background_image')) { ?>
		<section class="parallax" data-aos="fade-in" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_title'); ?></h2>
					<p><?php the_field('parallax_content'); ?></p>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>