<?php
/* Template Name: Clubspeed */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php
			$bannerImage = (get_field('banner_image')) ? 'background-image: url('.get_field('banner_image').');' : 'background-image: url(/wp-content/uploads/2017/06/generic-header-3.jpg);';
			$bannerText = (get_field('banner_text')) ? get_field('banner_text') : '';
			if (basename(get_page_template()) === 'page.php') {
				$bannerText = (get_field('banner_text')) ? get_field('banner_text') : get_the_title();
			}
			if (is_singular('press-room')) {
				$bannerImage = 'background-image: url('.get_the_post_thumbnail_url().');';
			}
		?>
		<section class="sub-banner" style="position: relative; <?php echo $bannerImage; ?>;">
			<div class="pattern-overlay" style="width: 100%; height: 100%; position: absolute; top: 0; left: 0;"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-12" data-aos="zoom-in-up" data-aos-duration="1000">
						<?php //if ($bannerText != '') { ?>
							<h2><?php echo $bannerText; ?></h2>
						<?php //} ?>
					</div>
				</div>
			</div>
		</section>

    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 472)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<a href="/race-results" style="text-decoration: none;"><h4 class="heading"><?php echo get_the_title(472); ?></h4></a>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>

		<section class="rules littleBigWrapper" style="padding-top: 0px;">
			<div class="container">
				<div id="post-<?php the_ID(); ?>" <?php post_class('text normalContent'); ?>>
					<?php if (is_page('race-results')): ?>
					<section class="nav-tabs">
						<!--<div class="container text-center">
							<ul class="nav">
								<li class="active nav1"><a href="#1" data-toggle="tab" class="tabs-btn2 button red" style="border:0;"><span>Online Registration</span></a></li>
								<li class="nav2"><a href="#2" data-toggle="tab" class="tabs-btn2 button red" style="border:0;"><span>Customer Portal</span></a></li>
							</ul>
						</div>-->
					</section>
					<?php else : ?>
					<section class="nav-tabs">
						<!--<div class="container text-center">
							<ul class="nav">
								<li class="active nav1"><a href="/race-results/?online-registration" class="tabs-btn2 button red" style="border:0;"><span>Online Registration</span></a></li>
								<li class="nav2"><a href="/race-results/?customer-portal" class="tabs-btn2 button red" style="border:0;"><span>Customer Portal</span></a></li>
							</ul>
						</div>-->
					</section>
					<?php endif; ?>

					<div class="location-heading clearfix" style="margin-top: 0px; margin-bottom: 20px; max-width: 100%;">
						<?php include 'template-part-location.php'; ?>
					</div>
					<?php if (!is_page('race-results')): ?>
                    <div style="margin-left:15px;" class="clearfix">
                        <div class="dop location-heading">
                            <h3 class="dop_h">TRACK</h3>
                            <a class="dop_a dropdown-toggle" data-toggle="dropdown" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false"><img src="https://rpmraceway.com/wp-content/themes/rpm-raceway/assets/images/errow.png" alt=""></a>
                             <ul id="track" class="dropdown-menu">
                                <li><a href="javascript:;" data-location="1">TRACK 1</a></li>
                                <li><a href="javascript:;" data-location="2">TRACK 2</a></li>
                                <li><a href="javascript:;" data-location="3">Super G</a></li>
                            </ul>
                        </div>
                        <div class="dop location-heading">
                            <h3 class="dop_h">SPEED LEVEL</h3>
                            <a class="dop_a dropdown-toggle" data-toggle="dropdown" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false"><img src="https://rpmraceway.com/wp-content/themes/rpm-raceway/assets/images/errow.png" alt=""></a>
                             <ul id="level" class="dropdown-menu">
                                <li><a href="javascript:;" data-location="1">Adult</a></li>
                                <li><a href="javascript:;" data-location="2">Junior</a></li>
                                <li><a href="javascript:;" data-location="3">NON-LICENSE</a></li>
                            </ul>
                        </div>
                    </div> 
					<?php endif ?>
					<?php if (is_page('race-results')): ?>
					<section class="tab-content ">
						<div class="tab-pane active" id="1">
							<iframe id="online-registration" src="" width="100%" height="800" frameborder="0" ></iframe>
							<script type="text/javascript">
								jQuery(function($){
									if (jQuery.cookie('visitorLocation') == 'Buffalo') {
										$('#online-registration').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/sp_center/login.aspx');
									}
									if (jQuery.cookie('visitorLocation') == 'Rochester') {
										$('#online-registration').attr('src', 'https://rpmrochester.clubspeedtiming.com/sp_center/login.aspx');
									}
									if (jQuery.cookie('visitorLocation') == 'Long Island') {
										$('#online-registration').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center/login.aspx');
									}
									if (jQuery.cookie('visitorLocation') == 'Stamford') {
										$('#online-registration').attr('src', 'https://rpmstamford.clubspeedtiming.com/sp_center/login.aspx');
									}
									if (jQuery.cookie('visitorLocation') == 'Syracuse') {
										$('#online-registration').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/sp_center/login.aspx');
									}
									if (jQuery.cookie('visitorLocation') == 'Jersey City') {
										$('#online-registration').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/sp_center/login.aspx');
									}
								});
							</script>
						</div>
						<div class="tab-pane" id="2">
							<iframe id="customer-portal" src="" width="100%" height="870" frameborder="0" ></iframe>
							<script type="text/javascript">
								jQuery(function($){
									if (jQuery.cookie('visitorLocation') == 'Buffalo') {
										$('#customer-portal').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/sp_center');
									}
									if (jQuery.cookie('visitorLocation') == 'Rochester') {
										$('#customer-portal').attr('src', 'https://rpmrochester.clubspeedtiming.com/sp_center');
									}
									if (jQuery.cookie('visitorLocation') == 'Long Island') {
										$('#customer-portal').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center');
									}
									if (jQuery.cookie('visitorLocation') == 'Stamford') {
										$('#customer-portal').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center');
									}
									if (jQuery.cookie('visitorLocation') == 'Syracuse') {
										$('#customer-portal').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/sp_center');
									}
									if (jQuery.cookie('visitorLocation') == 'Jersey City') {
										$('#customer-portal').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/sp_center');
									}
								});
							</script>
						</div>
					</section>
					<?php endif ?>                      
					<?php if (is_page('top-times-of-the-day')): ?>
						<iframe id="top-time-of-the-day" src="" width="100%" height="480" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
                                concat_url(jQuery.cookie('visitorLocation'),jQuery.cookie('track'),jQuery.cookie('level'),'');
							});
						</script>
					<?php endif ?>
					<?php if (is_page('top-times-of-the-week')): ?>
						<iframe id="top-time-of-the-week" src="" width="100%" height="480" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
								/*if (jQuery.cookie('visitorLocation') == 'Buffalo') {
									$('#top-time-of-the-week').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/sp_center/toptime.aspx?Days=7');
								}
								if (jQuery.cookie('visitorLocation') == 'Rochester') {
									$('#top-time-of-the-week').attr('src', 'https://rpmrochester.clubspeedtiming.com/sp_center/toptime.aspx?Days=7');
								}
								if (jQuery.cookie('visitorLocation') == 'Long Island') {
									$('#top-time-of-the-week').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center/toptime.aspx?Days=7');
								}
								if (jQuery.cookie('visitorLocation') == 'Syracuse') {
									$('#top-time-of-the-week').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/sp_center/toptime.aspx?Days=7');
								}
								if (jQuery.cookie('visitorLocation') == 'Jersey City') {
									$('#top-time-of-the-week').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/sp_center/toptime.aspx?Days=7');
								}*/
                                concat_url(jQuery.cookie('visitorLocation'),jQuery.cookie('track'),jQuery.cookie('level'),'7');
							});
						</script>
					<?php endif ?>
					<?php if (is_page('top-times-of-the-month')): ?>
						<iframe id="top-time-of-the-month" src="" width="100%" height="480" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
								/*if (jQuery.cookie('visitorLocation') == 'Buffalo') {
									$('#top-time-of-the-month').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/sp_center/toptime.aspx?Days=30');
								}
								if (jQuery.cookie('visitorLocation') == 'Rochester') {
									$('#top-time-of-the-month').attr('src', 'https://rpmrochester.clubspeedtiming.com/sp_center/toptime.aspx?Days=30');
								}
								if (jQuery.cookie('visitorLocation') == 'Long Island') {
									$('#top-time-of-the-month').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center/toptime.aspx?Days=30');
								}
								if (jQuery.cookie('visitorLocation') == 'Syracuse') {
									$('#top-time-of-the-month').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/sp_center/toptime.aspx?Days=30');
								}
								if (jQuery.cookie('visitorLocation') == 'Jersey City') {
									$('#top-time-of-the-month').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/sp_center/toptime.aspx?Days=30');
								}*/
                                concat_url(jQuery.cookie('visitorLocation'),jQuery.cookie('track'),jQuery.cookie('level'),'30');
							});
						</script>
					<?php endif ?>
					<?php if (is_page('top-proskill-scores')): ?>
						<iframe id="top-proskill-scores" src="" width="100%" height="3500" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
								if (jQuery.cookie('visitorLocation') == 'Buffalo') {
									$('#top-proskill-scores').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/sp_center/RPM.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Rochester') {
									$('#top-proskill-scores').attr('src', 'https://rpmrochester.clubspeedtiming.com/sp_center/RPM.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Long Island') {
									$('#top-proskill-scores').attr('src', 'https://rpmlongisland.clubspeedtiming.com/sp_center/RPM.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Stamford') {
									$('#top-proskill-scores').attr('src', 'https://rpmstamford.clubspeedtiming.com/sp_center/RPM.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Syracuse') {
									$('#top-proskill-scores').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/sp_center/RPM.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Jersey City') {
									$('#top-proskill-scores').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/sp_center/RPM.aspx');
								}
							});
						</script>
					<?php endif ?>
					<?php if (is_page('live-scoreboard')): ?>
						<iframe id="live-scoreboard" src="" width="100%" height="500" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
								if (jQuery.cookie('visitorLocation') == 'Buffalo') {
									$('#live-scoreboard').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Rochester') {
									$('#live-scoreboard').attr('src', 'https://rpmrochester.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Long Island') {
									$('#live-scoreboard').attr('src', 'https://rpmlongisland.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Stamford') {
									$('#live-scoreboard').attr('src', 'https://rpmstamford.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Syracuse') {
									$('#live-scoreboard').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
								if (jQuery.cookie('visitorLocation') == 'Jersey City') {
									$('#live-scoreboard').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/Sp_center/livescore.aspx');
								}
							});
						</script>
					<?php endif ?>
					<?php if (is_page('online-booking')): ?>
						<iframe id="online-booking" src="" width="100%" height="800" frameborder="0" ></iframe>
						<script type="text/javascript">
							jQuery(function($){
								if (jQuery.cookie('visitorLocation') == 'Buffalo') {
									$('#online-booking').attr('src', 'https://rpmbuffalo.clubspeedtiming.com/booking');
								}
								if (jQuery.cookie('visitorLocation') == 'Rochester') {
									$('#online-booking').attr('src', 'https://rpmrochester.clubspeedtiming.com/booking');
								}
								if (jQuery.cookie('visitorLocation') == 'Long Island') {
									$('#online-booking').attr('src', 'https://rpmlongisland.clubspeedtiming.com/booking');
								}
								if (jQuery.cookie('visitorLocation') == 'Stamford') {
									$('#online-booking').attr('src', 'https://rpmlongisland.clubspeedtiming.com/booking');
								}
								if (jQuery.cookie('visitorLocation') == 'Syracuse') {
									$('#online-booking').attr('src', 'https://rpmsyracuse.clubspeedtiming.com/booking');
								}
								if (jQuery.cookie('visitorLocation') == 'Jersey City') {
									$('#online-booking').attr('src', 'https://rpmjerseycity.clubspeedtiming.com/booking');
								}
							});
						</script>
					<?php endif ?>
            		<?php the_content(); ?>
				</div>
			</div>
		</section>

    <?php endwhile; endif; ?>
<style type="text/css">

.nav-tabs .button {
    width: 295px;
    height: 55px;
    color: #ffffff;
    font-family: "Droid Sans";
    font-size: 16px;
    font-weight: 700;
    line-height: 53px;
    position: relative;
    display: inline-block;
    margin: 25px 13px 0px 0px;
    text-align: left;
    padding-left: 15px;
    transition: all 0.3s;
    overflow: hidden;
    -webkit-transition: all .7s ease;
}
@media(max-width: 1024px) {
	.inner-nav li.active a:before,
	.inner-nav li.active a:after,
	.inner-nav li a:hover:before,
	.inner-nav li a:hover:after,
	.inner-nav .heading:before,
	.inner-nav .heading:after,
	.inner-nav li:after {
		display: none;
	}
	.inner-nav ul {
		margin: 0;
	}
	.inner-nav li {
		margin: 0;
		padding: 0;
		height: auto;
		line-height: 12px;
	}
	.inner-nav {
		display: block !important;
		height: auto;
		line-height: 24px;
	}
	.inner-nav ul > a {
		display: none;
	}
	.inner-nav,
	.inner-nav li,
	.inner-nav ul,
	.inner-nav ul a {
		background: #eee;
	}
	.inner-nav li a {
		margin: 0;
		padding: 0px 10px;
		line-height: 45px;
	}
}
@media(max-width: 768px) {
	.nav-tabs {
		margin-bottom: 15px;
	}
	.rules {
		padding-left: 0;
		padding-right: 0;
	}
	.nav-tabs .button {
		margin: 0 !important;
	}
	.nav-tabs li {
		padding: 0px !important;
	}
}
@media(max-width: 380px) {
	.nav-tabs li {
		width: 100%;
	}
	.nav-tabs .button {
		width: 100%;
		text-align: center;
	}
	.nav-tabs .button:after {
		display: none;
	}
}
</style>
<?php get_footer(); ?>


<script type="text/javascript">
jQuery(function(){
	jQuery('.sub-banner h2').html('<?php the_title(); ?>, '+jQuery.cookie('visitorLocation'));
	<?php if (isset($_GET['customer-portal'])) { ?>
		$('.nav1, #1').removeClass('active');
		$('.nav2, #2').addClass('active');
	<?php } ?>
	jQuery('.locationStuff ul li a').click(function(){
		jQuery('.sub-banner h2').html('<?php the_title(); ?>, '+jQuery(this).html());
        location.reload();
	});
    jQuery('#track a').on('click',function() {
        var cook = jQuery(this).attr('data-location');
        jQuery.cookie('track',cook);
        location.reload();
    });
    jQuery('#level a').on('click',function() {
        var cook = jQuery(this).attr('data-location');
        jQuery.cookie('level',cook);
        location.reload();
    });
    if(jQuery.cookie('level')) {
        if(jQuery.cookie('level')==1) {
            var t = 'Adult';
        }
        if(jQuery.cookie('level')==2) {
            var t = 'Junior';
        }
        if(jQuery.cookie('level')==3) {
            var t = 'Non-license';
        }
        jQuery('#level').parent().find('h3').text(t);
    };
    if(jQuery.cookie('track')) {
        jQuery('#track').parent().find('h3').text('Track ' + jQuery.cookie('track'));
        if(jQuery.cookie('track') == 3) {
            jQuery('#track').parent().find('h3').text('Super G');
        }
        if(jQuery.cookie('visitorLocation') =='Syracuse' || jQuery.cookie('visitorLocation') =='Rochester' || jQuery.cookie('visitorLocation') =='Buffalo' ) {
           jQuery('#track').parent().find('h3').text('Track 1'); 
        };
    };
    if (location.href.indexOf('customer-portal') > 0) {
        jQuery('.dop').css('display','none');
    };
        if (location.href == 'https://rpmraceway.com/race/race-results/' || location.href == 'https://rpmraceway.com/race/race-results/live-scoreboard/' || location.href == 'https://rpmraceway.com/race/race-results/top-proskill-scores/' ) {
        jQuery('.dop').css('display','none');
    };
});
    if(jQuery.cookie('visitorLocation') =='Syracuse' || jQuery.cookie('visitorLocation') =='Rochester' || jQuery.cookie('visitorLocation') =='Buffalo' ) {
          jQuery('#track li:eq(1),#track li:eq(2)').css('display','none');  
          jQuery('#track').parent().find('h3').text('Track 1');
    }
</script>