<?php
/* Template Name: Racing Leagues */
get_header();
if(get_field('redirect_url') != '') {
	wp_redirect(get_field('redirect_url'));
	exit;
}
?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 10)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading">RACING AT RPM</h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages league-sec clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/flag.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>

		<?php if( have_rows('race_leagues_pricing_cards') ) { ?>
		<section class="white-bg four-column">
			<div class="container text-center">
				<div class="row">
					<div class="location-heading clearfix">
						<?php include 'template-part-location.php'; ?>
					</div>
				</div>
				<div class="row grid">
						<?php while( have_rows('race_leagues_pricing_cards') ) { the_row(); ?>
							<div class="box grid-item <?php echo get_sub_field('location'); ?>">
								<?php if (get_sub_field('image')) { ?>
									<figure><img src="<?php the_sub_field('image'); ?>" class="img-responsive"></figure>
								<?php } ?>
								<div class="content-box">
									<?php if (get_sub_field('heading')) { ?>
										<h3><?php the_sub_field('heading'); ?></h3>
									<?php } ?>
									<?php if (get_sub_field('price')) { ?>
										<h2><?php the_sub_field('price'); ?></h2>
									<?php } ?>
									<?php if (get_sub_field('text_under_price')) { ?>
										<p class="text-center"><?php the_sub_field('text_under_price'); ?></p>
									<?php } ?>
									<?php if (get_sub_field('content')) { ?>
										<?php the_sub_field('content'); ?>
									<?php } ?>
									<?php if (get_sub_field('button_link')) { ?>
										<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
									<?php } ?>
									<?php if (get_sub_field('text_under_button')) { ?>
										<p><small><?php the_sub_field('text_under_button'); ?></small></p>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider" data-aos="fade-in" data-aos-duration="1000">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
			            <div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')";></div>
					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>