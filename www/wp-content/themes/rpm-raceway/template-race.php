<?php
/* Template Name: Race */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 10)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading">RACING AT RPM</h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/flag.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>
		<section class="black-bg" data-aos="fade-in" data-aos-duration="1000">
			<div class="container">
				<div class="row">
					<div class="col-md-1"></div>
					<div class="col-md-5">
						<?php the_field('black_area_content_left'); ?>
					</div>
					<div class="col-md-5">
						<?php the_field('black_area_content_right'); ?>
					</div>
				</div>
			</div>
		</section>

		<?php if( have_rows('race_driver_categories') ) { ?>
		<section class="driver-cat">
			<div class="container">
				<h4>race driver categories</h4>
				<figure class="line-break">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
				</figure>
				<p><?php the_field('race_driver_categories_text'); ?></p>
				<?php while( have_rows('race_driver_categories') ) { the_row(); ?>
					<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
						<h3><?php the_sub_field('title'); ?></h3>
						<?php the_sub_field('points'); ?>
						<figure>
							<img src="<?php the_sub_field('image'); ?>">
						</figure>
					</div>
				<?php } ?>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
			            <div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')";></div>
					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<section class="grey-bg">
			<div class="container text-center">
				<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php the_field('gray_area_content_left'); ?>
				</div>
				<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php the_field('gray_area_content_right'); ?>
				</div>
			</div>
		</section>

		<?php if (get_field('fasten_your_seatbelt_content')) { ?>
		<section class="rules" data-aos="fade-up" data-aos-duration="1000">
			<div class="container">
				<div class="text">
					<h4>fasten your seatbelt</h4>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
					</figure>
					<?php the_field('fasten_your_seatbelt_content'); ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if (get_field('parallax_background_image')) { ?>
		<section class="parallax" data-aos="fade-in" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<a href="<?php the_field('parallax_button_link'); ?>" class="button red"><span><?php the_field('parallax_button_text'); ?></span></a>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('racing_resources') ) { ?>
		<section class="resource">
			<div class="container text-center">
				<h4>racing resources</h4>
				<figure class="line-break">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
				</figure>
				<div class="btn-group clearfix">
					<?php while( have_rows('racing_resources') ) { the_row(); ?>
						<?php if (get_sub_field('link')) { ?>
							<a data-aos="fade-in-up" data-aos-duration="1000" href="<?php the_sub_field('link'); ?>" class="button grey"><span><?php the_sub_field('text'); ?></span></a>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</section>
		<?php } ?>
		<?php include 'template-part-bottom-nav.php'; ?>
		<br>
		<br>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>