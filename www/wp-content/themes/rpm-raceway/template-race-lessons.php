<?php
/* Template Name: Racing Lessons */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 10)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading">RACING AT RPM</h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/flag.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>

		<?php if( have_rows('racing_boxes') ) { ?>
		<section class="grey-bg three-column">
			<div class="container text-center">
			<?php while( have_rows('racing_boxes') ) { the_row(); ?>
				<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php if (get_sub_field('title')) { ?>
						<h3><?php the_sub_field('title'); ?></h3>
					<?php } ?>
					<p><?php the_sub_field('text'); ?></p>
					<?php if (get_sub_field('button_link')) { ?>
						<a href="<?php the_sub_field('button_link'); ?>" class="black-btn"><?php the_sub_field('button_text'); ?></a>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		</section>
		<?php } ?>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider" data-aos="fade-in" data-aos-duration="1000">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
			            <div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')";></div>
					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>