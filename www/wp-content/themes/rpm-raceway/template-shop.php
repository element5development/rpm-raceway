<?php
/* Template Name: Shop */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="white-bg shop-section">
			<div class="container">
				<h2 class="text-center">SHOP <span>RPM RACEWAY</span></h2>
				<div class="row">
					<div class="location-heading clearfix" style="margin-top: 20px;">
						<div class="pull-left">
							<h3>Shop by category</h3>
						</div>
						<div class="pull-right">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
							 <ul class="dropdown-menu">
							 	<li><a href="javascript:;">Category 1</a></li>
							 	<li><a href="javascript:;">Category 2</a></li>
							 	<li><a href="javascript:;">Category 3</a></li>
							 	<li><a href="javascript:;">Category 4</a></li>
	                        </ul>
						</div>
					</div>
					<div class="location-heading clearfix" style="margin-top: 20px;">
						<div class="pull-left">
							<h3>Filter results</h3>
						</div>
						<div class="pull-right">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
							 <ul class="dropdown-menu">
							 	<li><a href="javascript:;">Filter 1</a></li>
							 	<li><a href="javascript:;">Filter 2</a></li>
							 	<li><a href="javascript:;">Filter 3</a></li>
							 	<li><a href="javascript:;">Filter 4</a></li>
	                        </ul>
						</div>
					</div>
					<div class="location-heading clearfix" style="margin-top: 20px; margin-right: 0px;">
						<div class="pull-left">
							<h3>Highest to lowest price</h3>
						</div>
						<div class="pull-right">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
							 <ul class="dropdown-menu">
							 	<li><a href="javascript:;">Price 1</a></li>
							 	<li><a href="javascript:;">Price 2</a></li>
							 	<li><a href="javascript:;">Price 3</a></li>
							 	<li><a href="javascript:;">Price 4</a></li>
	                        </ul>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
					<div class="col-sm-4"  data-aos="zoom-in-up" data-aos-duration="1000">
						<img src="http://placehold.it/285x326" class="img-responsive">
						<h1>Product Title</h1>
						<h4>$0.00</h4>
					</div>
				</div>
			</div>
			<div class="container text-center">
				<div class="pagination">
					<a href="javascript:;" class="black-btn">previous</a>
					<a href="javascript:;" class="active">1</a>
					<a href="javascript:;">2</a>
					<a href="javascript:;">3</a>
					<a href="javascript:;">4</a>
					<a href="javascript:;">...16</a>
					<a href="javascript:;" class="black-btn">next</a>
				</div>
			</div>
		</section>

		<?php include 'template-part-reviews.php'; ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>