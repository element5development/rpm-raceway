<?php query_posts('post_per_page=-1&post_type=rpm_reviews'); ?>
		<section class="black-bg three-column" data-aos="fade-in-up" data-aos-duration="1000">
			<div class="container">
				<div class="text-center">
					<h4>REVIEWS</h4>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
					</figure>
				</div>
				<div class="review owl-carousel owl-theme">
					<?php while ( have_posts() ) { the_post(); ?>
						<div class="box">
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<i class="fa fa-star"></i>
							<?php the_content(); ?>
							<h6><?php the_title(); ?></h6>
						</div>
					<?php } ?>
				</div>
			</div>
		</section>
<?php wp_reset_query(); ?>