<?php
/* Template Name: Contact */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
		<section class="white-bg four-column corporate-events parties">
			<div class="container text-center">
				<div class="row">
					<div class="location-heading clearfix" style="margin-top: 70px; margin-bottom: 0px;">
						<?php include 'template-part-location.php'; ?>
					</div>
				</div>
			</div>
		</section>
		<section class="map-section clearfix">
			<div class="map-area" data-aos="fade-right" data-aos-duration="1000" style="background-color: gray;">
				<div class="JerseyCity mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.789944400864!2d-74.07405098508815!3d40.70062364624926!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c250e37a00490d%3A0xe7551242a7fb7e75!2s99+Caven+Point+Rd%2C+Jersey+City%2C+NJ+07305!5e0!3m2!1sen!2s!4v1497527315901" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="LongIsland mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3022.70544671194!2d-73.42799798494134!3d40.746506479328076!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e82afba46d3c17%3A0x6148e07f35c824de!2s40+Daniel+St%2C+Farmingdale%2C+NY+11735!5e0!3m2!1sen!2s!4v1498588093111" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="Stamford mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3009.4855068240117!2d-73.56141818474498!3d41.03651072563621!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c29f4d2b0802e1%3A0x2f45a858c7b1ad05!2s600+West+Ave%2C+Stamford%2C+CT+06902!5e0!3m2!1sen!2s!4v1527181888530" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="Buffalo mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2922.142949648771!2d-78.76562858486189!3d42.91202607915362!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d30cbd967f9139%3A0x3c88df108e12e8e6!2s1+Walden+Galleria%2C+Buffalo%2C+NY+14225!5e0!3m2!1sen!2s!4v1498588146238" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="Rochester mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2914.033227515701!2d-77.63940328452064!3d43.08279677914496!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d14b766d602263%3A0xa9a16b4ca59c8c25!2s17+Miracle+Mile+Dr%2C+Rochester%2C+NY+14623!5e0!3m2!1sen!2sus!4v1498703681205" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="Syracuse mapIframe">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2914.6549380078795!2d-76.17579283485595!3d43.06972437914563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d9f2295b4b330d%3A0x474f0dc63d198428!2s9090+Destiny+USA+Dr%2C+Syracuse%2C+NY+13204!5e0!3m2!1sen!2s!4v1498588211756" width="100%" height="282" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="map-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content text-center">
					<h4>our track</h4>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small-red.png">
					</figure>
					<a href="tel:201-333-7223" class="JerseyCity map-btn">
						<i class="fa fa-phone"></i> 201-333-7223
					</a>
					<a href="tel:631-752-7223" class="LongIsland map-btn">
						<i class="fa fa-phone"></i> 631-752-7223
					</a>
					<a href="tel:203-323-7223" class="Stamford map-btn">
						<i class="fa fa-phone"></i> 203-323-7223
					</a>
					<a href="tel:716-683-7223" class="Buffalo map-btn">
						<i class="fa fa-phone"></i> 716-683-7223
					</a>
					<a href="tel:585-427-7223" class="Rochester map-btn">
						<i class="fa fa-phone"></i> 585-427-7223
					</a>
					<a href="tel:‎315-423-7223" class="Syracuse map-btn">
						<i class="fa fa-phone"></i> ‎315-423-7223
					</a>

					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'jersey_city_address' ); ?>" target="_blank" class="JerseyCity map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'jersey_city_address' ); ?>
					</a>
					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'long_island_address' ); ?>" target="_blank" class="LongIsland map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'long_island_address' ); ?>
					</a>
					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'stamford_address' ); ?>" target="_blank" class="Stamford map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'stamford_address' ); ?>
					</a>
					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'buffalo_address' ); ?>" target="_blank" class="Buffalo map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'buffalo_address' ); ?>
					</a>
					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'rochester_address' ); ?>" target="_blank" class="Rochester map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'rochester_address' ); ?>
					</a>
					<a href="http://maps.google.com/?q=<?php echo ot_get_option( 'syracuse_address' ); ?>" target="_blank" class="Syracuse map-btn">
						<i class="fa fa-map-maker"></i> <?php echo ot_get_option( 'syracuse_address' ); ?>
					</a>

					<h4 style="margin-top: 30px;">HOURS</h4>
					<div class="calendarPage JerseyCity"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'jersey_city_hours' ); ?>
					</span></div>
					<div class="calendarPage Stamford"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'long_island_hours' ); ?>
					</span></div>
					<div class="calendarPage LongIsland"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'long_island_hours' ); ?>
					</span></div>
					<div class="calendarPage Buffalo"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'buffalo_hours' ); ?>
					</span></div>
					<div class="calendarPage Rochester"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'rochester_hours' ); ?>
					</span></div>
					<div class="calendarPage Syracuse"><span style="font-size: 14px; color: #fff;">
						<?php echo ot_get_option( 'syracuse_hours' ); ?>
					</span></div>
					<style type="text/css">.calendarPage p { color: white; font-weight: normal; font-size: 14px; display: inline-block;} </style>
				</div>
			</div>
		</section>
		<section class="parties-content contact" data-aos="zoom-in-up" data-aos-duration="1000">
			<div class="container text-center">
				<?php the_content(); ?>
			</div>
		</section>

		<?php if( have_rows('image_slider') ) { ?>
		<section class="full-width-slider" data-aos="fade-up" data-aos-duration="1000">
			<div class="full-width owl-carousel owl-theme">
					<?php while( have_rows('image_slider') ) { the_row(); ?>
			            <div class="item" style="background-image: url('<?php echo get_sub_field('image'); ?>')";></div>
					<?php } ?>
	      	</div>
		</section>
		<?php } ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>