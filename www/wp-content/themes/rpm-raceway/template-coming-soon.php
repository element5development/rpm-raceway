<?php
/* Template Name: Coming Soon */
?>
<html style="height: 100%; overflow: hidden;">
<head>
	<title>RPM Raceway - COMING SOON</title>
	<link rel="shortcut icon" href="/wp-content/themes/rpm-raceway/assets/images/rpm-favicon.png" type="image/x-icon">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
	<style type="text/css">
		@import url('https://fonts.googleapis.com/css?family=Droid+Sans:400,700');
		* {
			font-family: 'Droid Sans';
		}
/*		a:hover {
			background: #d81212 !important;
			border-color: #d81212 !important;
			color: #fff !important;
		}*/

		.footer-social a {
		    display: inline-block;
		}

.footer-social a {
    display: inline-block;
    margin-left: 10px;
    border: 3px solid #d81212;
    border-radius: 100%;
    width: 41px;
    height: 41px;
    color: #fff;
    font-size: 22px;
    padding: 6px 0;
    text-align: center;
    box-sizing: border-box;
}
.footer-social a:hover {
    background: #d81212;
}
	</style>
</head>
<body style="background: #000; height: 100%; display: flex; width: 100%; align-items: center; overflow: hidden;">
<div style="align-items: center; text-align: center; width: 100%;">
	<img class="logo-sticky" style="max-width: 700px;margin-bottom: 40px;" src="/wp-content/themes/rpm-raceway/assets/images/long-logo.svg">
	<h1 style="color: #d81212; text-transform: uppercase; margin-top: 0px; font-size: 50px; font-weight: bold; ">Coming Soon</h1>
	<h2 style="color: #fff; font-size: 24px; font-weight: normal; margin-bottom: 20px;">Stay Connected</h2>
<div class="footer-social">
<a href="https://business.facebook.com/RPM-Raceway-1936749163225267/?business_id=1033394223458793&amp;ref=bookmarks" target="_blank"><i class="fa fa-facebook"></i></a>
<a href="https://www.instagram.com/rpmraceway/" target="_blank"><i class="fa fa-instagram"></i></a>
<a href="https://twitter.com/RPMRaceway?lang=en" target="_blank"><i class="fa fa-twitter"></i></a>
</div>
</div>
</body>
</html>