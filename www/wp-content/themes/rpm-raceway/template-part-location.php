
<div class="pull-left">
	<h3><i class="fa fa-map-marker"></i> Change your location</h3>
    

</div>
<div class="pull-right locationStuff">
	<h3><?php echo (isset($_COOKIE['visitorLocation'])) ? $_COOKIE['visitorLocation'] : ''; ?></h3>
	<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php bloginfo('template_url'); ?>/assets/images/errow.png" alt=""></a>
	 <ul class="dropdown-menu">
	 	<li><a href="javascript:;" data-location=".JerseyCity">Jersey City</a></li>
	 	<li><a href="javascript:;" data-location=".LongIsland">Long Island</a></li>
	 	<li><a href="javascript:;" data-location=".Stamford">Stamford</a></li>
	 	<li><a href="javascript:;" data-location=".Buffalo">Buffalo</a></li>
	 	<li><a href="javascript:;" data-location=".Rochester">Rochester</a></li>
	 	<li><a href="javascript:;" data-location=".Syracuse">Syracuse</a></li>
    </ul>
</div>