<?php
/* Template Name: Group Events */
get_header(); ?>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    	<?php include 'template-part-banner.php';  ?>
    	<?php $allPages = get_pages(array('sort_order' => 'asc', 'sort_column' => 'menu_order', 'post_type' => 'page', 'post_status' => 'publish', 'parent' => 11)); ?>
		<section class="inner-nav">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<ul>
							<h4 class="heading"><?php echo get_the_title(11); ?></h4>
							<?php foreach ($allPages as $key => $value) {
								$activeClass = (strpos($_SERVER["REQUEST_URI"], $value->post_name) !== false) ? "active" : "";
								echo '<li class="'.$activeClass.'"><a href="/'.$value->post_name.'">'.$value->post_title.'</a></li>';
							} ?>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="content-with-image inner-pages clearfix">
			<div class="left-content" data-aos="fade-right" data-aos-duration="1000">
				<div class="circle">
					<img src="<?php bloginfo('template_url'); ?>/assets/images/users.png" alt="">
				</div>
				<figure class="full-image" style="background-image: url(<?php the_field('left_image'); ?>);">
				</figure>
			</div>
			<div class="right-content" data-aos="fade-left" data-aos-duration="1000">
				<div class="content">
					<?php the_field('right_content_heading'); ?>
					<figure class="line-break">
						<img src="<?php bloginfo('template_url'); ?>/assets/images/line-break-small.png" alt="">
					</figure>
					<?php the_field('right_content_text'); ?>
				</div>
			</div>
		</section>

		<?php if( have_rows('racing_boxes') ) { ?>
		<section class="grey-bg three-column group-events">
			<div class="container text-center">
			<?php while( have_rows('racing_boxes') ) { the_row(); ?>
				<div class="box" data-aos="zoom-in-up" data-aos-duration="1000">
					<?php if (get_sub_field('title')) { ?>
						<h3><?php the_sub_field('title'); ?></h3>
					<?php } ?>
					<p><?php the_sub_field('text'); ?></p>
					<?php if (get_sub_field('button_link')) { ?>
					<div class="text-center">
						<a href="<?php the_sub_field('button_link'); ?>" class="red-btn"><?php the_sub_field('button_text'); ?></a>
					</div>
					<?php } ?>
				</div>
			<?php } ?>
			</div>
		</section>
		<?php } ?>

		<?php if(get_field('parallax_background_image')) { ?>
		<section class="parallax" data-aos="fade-up" data-aos-duration="1000" style="background-image: url(<?php the_field('parallax_background_image'); ?>);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<h2><?php the_field('parallax_heading'); ?></h2>
					<a href="<?php the_field('parallax_button_1_link'); ?>" class="button red"><span><?php the_field('parallax_button_1_text'); ?></span></a>
					<?php if (get_field('parallax_button_2_link')) { ?>
						<a href="<?php the_field('parallax_button_2_link'); ?>" class="button black"><span><?php the_field('parallax_button_2_text'); ?></span></a>
					<?php } ?>
				</div>
			</div>
		</section>
		<?php } ?>

		<?php include 'template-part-reviews.php'; ?>

		<?php include 'template-part-bottom-nav.php'; ?>

    <?php endwhile; endif; ?>

<?php get_footer(); ?>